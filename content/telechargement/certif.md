title:  Certification of downloaded files
lang: en
slug:     certif
author:      Georges Khaznadar
date:        2024-02-06
category: Téléchargement

How to certify downloaded files
===============================

When one image is downloaded, either "simple" or "heavy", as explained
in the [download](telecharge_utilise-en.html) page,
one must verify whether the downloaded GNU/Linux system can be
trusted.

The verification process is based on control sums and a digital signature.

Verifying the control sums
--------------------------

One must download the file ``sha512sums.txt`` neighboring the image;
this short file contains control sums, like this:

    6a6b7cc536951994cad5fe22291b7dffc5c6c7321e8004926296c9c4da81d8da5e7bbf3b186a3c3da28ab09e68a9786ae77337df2b51baeb779b0bd8fc6b417e  live-image-amd64.contents
	047ad2f3c99c71339e207a49b3fc8ed81c2ac7c88d8828518db89d78e0ee3ebdebb4956580c675746dc22d51262b57dccd19a1b03a1157e57927a3bfc712f9c3  live-image-amd64.files
	40af15e0a3084a486ec09c43f39c8cc1fba5129f96f699d3ce438d2b6424bb922bd4e164d31364beb9f6fc33177e3fb82bd72f5453d08895829f7ca9ff4a856a  live-image-amd64.hybrid.iso
	9c59c0cd155a4031daf49556199f5666933ae6463fea535e10d0d8f2bc43e8765c598ef544220a845acffcfb2d5d8495ce7ed69d8d4d59df03d7fb771c0db17c  live-image-amd64.hybrid.iso.zsync
	26178458cf951b313488a01a49d7fc39108a9719d9feae64ab0cfdff9cd557a697683e3d0da9027bc188c46fcc743fcf748ec6e3412c655481cbda179bbfeb45  live-image-amd64.packages
	68770d4061d345cbf26b21105d0cb1b790623dedb3ff8d20eacdda709342c650643090108ce89d3192b357edf2683bd6cd4c8e48adc683248a24d9fcd94709e4  live-image-amd64.stick16G.img.gz
	4b3f956eb0ac6f7afd5e1ee49e68cf5545f8ebe2c4dc21705bc20e829fbbf1680bd183f8f5ef30693198b37ef9bb7fe526b1d65822dc6f296a0d9cfa49be68a8  live-image-source.debian.contents
	03b20e7f279173e05e6fa6b490fe2e51a746358803f8f6ff8ae836e29252b6b33f48be03d07c712e5f3f479dfbe3fb6544a02119f834c9d0d7eb924e62be6330  live-image-source.debian.tar
	03aada89009c84337eacd8a0b89b31aca120153ff23dbf3465626498cf7b0934fcc321b3b5688de0846b4cb7ee642b79c94784970cca7812822499d145b6f2ac  live-image-source.live.contents
	dcfb7a4fd4262f4cfbd859fb0ce6e5c2c0df85946991ead6271a85a773c810e7562de39fda4b9219febacba3ec49f6542d3cba0a1496da308d922e339c551da3  live-image-source.live.tar

In order to check an image file, for example to check the file
``live-image-amd64.hybrid.iso`` which was downloaded, ensure the ISO file
and the control sum file are in the current directory; then, 
launch the command:

    sha512sum --check sha512sums.txt 2>/dev/null

I outputs a few lines, some of them complaining about missing files which
could not be opened, and at least one line, saying that the checksum is correct
for the file to be verified. If no such line is present, then the
downloaded image cannot be trusted.


Verifying the signature
-----------------------

Okay, but... if the very control sums cannot be trusted? One can always
fear that some impostor sneaked some malware into the image files, and
provided control sums coherent with those files.

The original author of Freeduc images, Georges Khaznadar (<georgesk@debian.org>),
digitally signed the file ``sha512sums.txt``. In order to check the signature,
one must first fetch the file  ``sha512sums.txt.asc``,
which contains something like that:

    -----BEGIN PGP SIGNATURE-----

	iQIzBAABCAAdFiEEM0CzZP9nFT+3zK6FHCgWkHE2rjkFAmNyJzYACgkQHCgWkHE2
	rjn0zBAAg0wOCozg/k1pRUn1uzRIN/rBa9OjNG+OuYet+f3H9Rqs5s/pA4cyKTIS
	vB588YnJ915UJ114keo6OdF7Le8LlWAcHUma7BRTGH6hTFi95m26lboVq+/z3w9J
	vyBQG927apDK26Brn0CR6oP/zMGzxbpzX3xrKnBDUN7QI4oIbl+lEiB+YAMhJEfQ
	ROcOLap+1ROk64LPKJKj9RvY35X3YtuH3WbkWUMvrT5ZzsDT6GVFDHPwdHsHCcOq
	3FUqgdOT0xf0RAB4SsWLaD6TRbkNhuiQuHBUdGW3FM0hUMzHuKUlWf8PucQdaFVD
	3vkQkTw/sBLkYn8eFKtAwBTo11NFSWU1fY6rjceCUX7nYfedB9LIsjGz/mOgOzPM
	HChLq9x4o1/HeQrJKYBiuXVAanuVcnvftNl9JhDyOQ3yY/pkzElKjEAx9c48trvj
	eXJXYJ+RU6aqZECX8n3Chx+slfPpAMulDMVeWZgsQXoH3Cjb9qrG5aj71rAjmyZe
	h2B9wHX02JvKqlUQeifko6LF1sqkajripSUxkg2bUPDLcKMWpVlS1c7QxX8ftGnc
	N0yvu8K+KrURqGMg+iqu2Z6uH8A+PX6e4xxaTiDgJ63Zgff/Tyt10Zo/jnbZhFyZ
	CITP2f72igu7z8W+Q1AE0imlOsn+zrbNUHgNxcoJSRP03Ue4+ok=
	=C6mg
	-----END PGP SIGNATURE-----

Then, one can check that the signature is coherent with the control sum file,
in the directory which contains both files, with the command:

    gpg --verify sha512sums.txt.asc
	
If everything is OK, here are the expected messages:

    gpg: assuming signed data in 'sha512sums.txt'
	gpg: Signature made Tue Feb  6 15:13:49 2024 CET
	gpg:                using RSA key 3340B364FF67153FB7CCAE851C2816907136AE39
	gpg: please do a --check-trustdb
	gpg: Good signature from "Georges Khaznadar <georgesk@debian.org>"
	gpg:                 aka "Georges Khaznadar <georges.khaznadar@free.fr>"
	gpg:                 aka "[jpeg image of size 699]"

"Good signature from" means that the signature is OK, and that nobody and
nothing has altered the contents of the control sum file.

Finally, trust or no trust?
===========================

The last part of the trust chain is *that one*:


the signing key ``RSA 3340B364FF67153FB7CCAE851C2816907136AE39`` which
belongs to « Georges Khaznadar <georgesk@debian.org> » can be trusted,
or not?

The answer to this question is: check whether this signing key is part of
the Debian keyring; every Debian developer owns a digital signing key,
and those developers are using GPG to build a trust web, each by
signing the keys of some others. If the signing key
``RSA 3340B364FF67153FB7CCAE851C2816907136AE39`` is part of the keyring,
then is can be trusted with the same trust level than the Debian organization.

It can be verified immediately, when one runs an official Debian
distribution.

Launch the command:

    gpg --keyring /usr/share/keyrings/debian-keyring.gpg --list-key 3340B364FF67153FB7CCAE851C2816907136AE39
	
If the output does not contain ``gpg: error reading key``, it's OK.
One can even see the expiration date of that signing key, which must be
in the future.

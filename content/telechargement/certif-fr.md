title:  Certification des données téléchargées
lang: fr
slug:     certif
author:      Georges Khaznadar
date:        2024-02-06
category: Téléchargement

Comment certifier les fichiers téléchargés
==========================================

Quand on a fini de télécharger une des images, « simple » ou « lourde »,
de la façon expliquée à la page de [téléchargement](telecharge_utilise.html),
il faut pouvoir vérifier que le système GNU/Linux ainsi récupéré est
digne de confiance.

La méthode de vérification fait appel aux sommes de contrôle et à la signature
électronique.

Vérification de la somme de contrôle
------------------------------------

On aura téléchargé le fichier ``sha512sums.txt`` en plus de l'image ;
ce fichier, très court, contient des sommes de contrôle, comme ceci :

    6a6b7cc536951994cad5fe22291b7dffc5c6c7321e8004926296c9c4da81d8da5e7bbf3b186a3c3da28ab09e68a9786ae77337df2b51baeb779b0bd8fc6b417e  live-image-amd64.contents
	047ad2f3c99c71339e207a49b3fc8ed81c2ac7c88d8828518db89d78e0ee3ebdebb4956580c675746dc22d51262b57dccd19a1b03a1157e57927a3bfc712f9c3  live-image-amd64.files
	40af15e0a3084a486ec09c43f39c8cc1fba5129f96f699d3ce438d2b6424bb922bd4e164d31364beb9f6fc33177e3fb82bd72f5453d08895829f7ca9ff4a856a  live-image-amd64.hybrid.iso
	9c59c0cd155a4031daf49556199f5666933ae6463fea535e10d0d8f2bc43e8765c598ef544220a845acffcfb2d5d8495ce7ed69d8d4d59df03d7fb771c0db17c  live-image-amd64.hybrid.iso.zsync
	26178458cf951b313488a01a49d7fc39108a9719d9feae64ab0cfdff9cd557a697683e3d0da9027bc188c46fcc743fcf748ec6e3412c655481cbda179bbfeb45  live-image-amd64.packages
	68770d4061d345cbf26b21105d0cb1b790623dedb3ff8d20eacdda709342c650643090108ce89d3192b357edf2683bd6cd4c8e48adc683248a24d9fcd94709e4  live-image-amd64.stick16G.img.gz
	4b3f956eb0ac6f7afd5e1ee49e68cf5545f8ebe2c4dc21705bc20e829fbbf1680bd183f8f5ef30693198b37ef9bb7fe526b1d65822dc6f296a0d9cfa49be68a8  live-image-source.debian.contents
	03b20e7f279173e05e6fa6b490fe2e51a746358803f8f6ff8ae836e29252b6b33f48be03d07c712e5f3f479dfbe3fb6544a02119f834c9d0d7eb924e62be6330  live-image-source.debian.tar
	03aada89009c84337eacd8a0b89b31aca120153ff23dbf3465626498cf7b0934fcc321b3b5688de0846b4cb7ee642b79c94784970cca7812822499d145b6f2ac  live-image-source.live.contents
	dcfb7a4fd4262f4cfbd859fb0ce6e5c2c0df85946991ead6271a85a773c810e7562de39fda4b9219febacba3ec49f6542d3cba0a1496da308d922e339c551da3  live-image-source.live.tar

Pour vérifier une image, par exemple pour vérifier le fichier 
``live-image-amd64.hybrid.iso`` qu'on vient de télécharger, on s'assure
que le fichier ISO et les sommes de contrôle sont dans un même répertoire ;
alors, dans ce répertoire, on lance la commande :

    sha512sum --check sha512sums.txt 2>/dev/null
	
On lit alors plusieurs lignes, certaines signalant des fichiers absents
qui ne peuvent pas être ouverts, et au moins un ligne, qui dit que la
vérification est réussie, pour le fichier qu'on veut contrôler. Si la ligne
du fichier à vérifier dit que la vérification n'est pas correcte, c'est
que le fichier téléchargé n'est pas digne de confiance.


Vérification de la signature
----------------------------

Oui, mais... et si les somme de contrôle étaient elles-même, indignes de
confiance ? on peut toujours craindre qu'un imposteur ait fabriqué
des images GNU/Linux malveillantes, et donné les sommes de contrôle
de ces fichiers-là.

L'auteur original des images Freeduc, Georges Khaznadar (<georgesk@debian.org>),
a signé numériquement le fichier ``sha512sums.txt``. Pour contrôler la
signature, il faut tout d'abord récupérer le fichier ``sha512sums.txt.asc``,
dont le contenu ressemble à quelque chose comme ça :

    -----BEGIN PGP SIGNATURE-----

	iQIzBAABCAAdFiEEM0CzZP9nFT+3zK6FHCgWkHE2rjkFAmNyJzYACgkQHCgWkHE2
	rjn0zBAAg0wOCozg/k1pRUn1uzRIN/rBa9OjNG+OuYet+f3H9Rqs5s/pA4cyKTIS
	vB588YnJ915UJ114keo6OdF7Le8LlWAcHUma7BRTGH6hTFi95m26lboVq+/z3w9J
	vyBQG927apDK26Brn0CR6oP/zMGzxbpzX3xrKnBDUN7QI4oIbl+lEiB+YAMhJEfQ
	ROcOLap+1ROk64LPKJKj9RvY35X3YtuH3WbkWUMvrT5ZzsDT6GVFDHPwdHsHCcOq
	3FUqgdOT0xf0RAB4SsWLaD6TRbkNhuiQuHBUdGW3FM0hUMzHuKUlWf8PucQdaFVD
	3vkQkTw/sBLkYn8eFKtAwBTo11NFSWU1fY6rjceCUX7nYfedB9LIsjGz/mOgOzPM
	HChLq9x4o1/HeQrJKYBiuXVAanuVcnvftNl9JhDyOQ3yY/pkzElKjEAx9c48trvj
	eXJXYJ+RU6aqZECX8n3Chx+slfPpAMulDMVeWZgsQXoH3Cjb9qrG5aj71rAjmyZe
	h2B9wHX02JvKqlUQeifko6LF1sqkajripSUxkg2bUPDLcKMWpVlS1c7QxX8ftGnc
	N0yvu8K+KrURqGMg+iqu2Z6uH8A+PX6e4xxaTiDgJ63Zgff/Tyt10Zo/jnbZhFyZ
	CITP2f72igu7z8W+Q1AE0imlOsn+zrbNUHgNxcoJSRP03Ue4+ok=
	=C6mg
	-----END PGP SIGNATURE-----

On peut alors vérifier la cohérence de la signature, dans le répertoire
qui contient tout à la fois les fichiers ``sha512sums.txt``et 
``sha512sums.txt.asc``, grâce à la commande :

    gpg --verify sha512sums.txt.asc
	
Si tout va bien, on voit alors les messages suivants :

    gpg: Signature faite le mar. 06 févr. 2024 15:13:49 CET
	gpg:                avec la clef RSA 3340B364FF67153FB7CCAE851C2816907136AE39
	gpg: veuillez faire un --check-trustdb
	gpg: Bonne signature de « Georges Khaznadar <georgesk@debian.org> »
	gpg:                 alias « Georges Khaznadar <georges.khaznadar@free.fr> »
	gpg:                 alias « [jpeg image of size 699] »

« Bonne signature de ... » signifie que la signature est conforme, et que
personne ni aucun processus de téléchargement n'a altéré le contenu
des sommes de contrôle.

Alors, confiance, ou pas confiance ?
====================================

Le dernier maillon de la chaîne de confiance, est le suivant :

la clé de chiffrement ``RSA 3340B364FF67153FB7CCAE851C2816907136AE39`` qui
appartient à « Georges Khaznadar <georgesk@debian.org> » est-elle digne de
confiance, ou pas ?

La réponse à cette question, c'est : vérifiez que cette clé de chiffrement
fait partie du trousseau de clés des développeurs Debian : tous les 
développeurs Debian ont une clé de chiffrement, et ils utilisent GPG pour
assurer le réseau de confiance, par un jeu de signatures croisées. Si la clé
``RSA 3340B364FF67153FB7CCAE851C2816907136AE39`` fait partie du trousseau,
alors elle est digne de recevoir la même confiance que celle qu'on prête
à l'organisation Debian.

C'est immédiat à vérifier, si on a sous la main une distribution
Debian officielle, en cours de fonctionnement :

On lance la commande :

    gpg --keyring /usr/share/keyrings/debian-keyring.gpg --list-key 3340B364FF67153FB7CCAE851C2816907136AE39
	


S'il y a une réponse autre que ``gpg: error reading key``, c'est bon. On
peut même vérifier la validité de la clé (sa date limite est indiquée,
elle doit être dans le futur).

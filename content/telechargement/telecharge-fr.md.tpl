title:  Téléchargement et utilisation sous GNU/Linux ou Windows
lang: fr
slug:     telecharge_utilise
author:      Georges Khaznadar
date:        {{ date }}
category: Téléchargement



Première étape : téléchargement de l'image comprimée
====================================================

Le téléchargement consiste à récupérer une image du système GNU/Linux ;
la taille de ces images convient pour un DVD-ROM (capacité 4,7 Go) ou une
clé USB (utiliser 16 Go au moins, de préférence).

L'image dite « lourde » est faite pour se décomprimer sur 16 giga-octets,
elle ne convient qu'aux clés USB. L'image « simple » convient dans tous 
les cas, mais elle ne traite pas tout de suite de la question des 
*données persistantes*.

<table>
<tr style="border: none; background: rgba(255,255,255,0);">
<td style="vertical-align: top; border: none;">
<ul id="images-gz">
{% for dist in distros %}
<li class="onglet-date"
    data-name="{{ dist.name }}"
    data-dir="{{ dist.basedir }}/{{ dist.dir }}"
    data-size="{{ dist.isofile.size or dist.hddfile.size }}"
    data-stick="{{ dist.stickimage.name }}"
    data-isoimage="{{ dist.isofile.name or dist.hddfile.name }}"
    data-sum="{{ dist.sums.name }}"
    data-sign="{{ dist.signature.name }}"
    >{{ dist.name }}</li>
{% endfor %}
</ul>
</td>
<td style="vertical-align: top; border: none;">
<div id="links" class="multicol quatre">
  <div class="titre-grid col1">
    Image « lourde » pour clé USB de 16 Go
  </div>
  <div class="titre-grid col2">
    Image « simple » pour clé USB ou DVD
  </div>
  <div class="titre-grid col3">
    Sommes de contrôle
  </div>
  <div class="titre-grid col4">
    Signature des sommes de contrôle
  </div>
  <div class="stick alink col1">
	  <div class="centerlink">
      <a href="#">
		  <img src="{attach}/img/download.png" title="l'image lourde" alt="lien vers l'image lourde"/><br>
		  <span></span>
	  </a>
	  </div>
  </div>
  <div class="isoimage alink col2">
	  <div class="centerlink">
	    <a href="#">
		  <img src="{attach}/img/download.png" title="l'image simple" 
		      alt="lien vers l'image simple" style="width: 120px;"/>
		  <img src="{attach}/img/download2.png" title="l'image simple" 
		      alt="lien vers l'image simple" style="width: 120px;"/>
	      <br>
		  <span></span>
	   </a>
	</div>
  </div>
  <div class="sum alink col3">
	<div class="centerlink">
        <a href="#">
		  <img src="{attach}/img/checksums.png" title="somme de contrôle"
		       alt="lien vers la somme de contrôle"/>
	    </a>
	</div>
  </div>
  <div class="sign alink col4">
	<div class="centerlink">
      <a href="#">
        <img src="{attach}/img/signatures.png" title="signature" alt="lien vers la signature"/>
      </a>
	</div>
  </div>
</div>
</td>
</tr>
</table>

<script type="text/javascript" src="/theme/js/telecharge.js">
</script>

... Bon, voilà, c'est téléchargé ... Mais comment peut-on faire confiance
au gros fichier qu'on vient de recevoir ? Pour cela, on peut consulter
la page au sujet de la [certification](certif.html).

Deuxième étape : gravure de l'image sur un support
==================================================

Deux cas sont à distinguer, image *lourde* ou *simple*.

Si c'est une image « lourde »
-----------------------------

On procède en deux temps :

* on décomprime le fichier ``live-image-amd64.stick16G.img.gz``, pour obtenir
  un fichier ``live-image-amd64.stick16G.img``, qui pèse environ 16 Go
* on copie ou plutôt on *grave* le fichier ``live-image-amd64.stick16G.img``
  sur la clé USB, dont la capacité doit être suffisante. Attention, si la
  clé a une capacité supérieure, 32 Go par exemple, seuls les premiers 16 Go
  de la clé seront gérés.
	
La **décompression** se fait avec tout bon gestionnaire d'archive, par exemple
``file-roller`` sous Linux, ``7zip`` sous Windows, etc.

La **gravure** n'est pas équivalente à une copie de fichier ordinaire :
l'image contient en effet des données qui doivent être placées dans le
secteur de démarrage (secteur de *boot*) du support de données. 

Pour faire ça, on peut utiliser, sous Windows, le logiciel libre et
gratuit [Win32 Disk
Imager](https://sourceforge.net/projects/win32diskimager/files/Archive/win32diskimager-1.0.0-install.exe/download).

Sous Linux, l'utilitaire ``gnome-disks`` (*paquet gnome-disk-utility*)
fait l'affaire si on veut disposer d'une interface graphique, sinon on
peut lancer une commande en ligne : 

    sudo dd if=<le_chemin_vers>live-image-amd64.stick16G.img of=/dev/<sdX> bs=4M conv=fdatasync status=progress

... où *le_chemin_vers* est l'indication du chemin d'accès au fichier
d'image (rien du tout si l'image est dans le répertoire par défaut
quand la commande est lancée), et *sdX* désigne la clé USB qu'on
branchée pour y graver l'image.

À tout moment, la commande ``lsblk`` permet de consulter la liste des
disques auxquelles il est possible d'accéder ; quand on branche la clé
USB, quelques lignes en plus apparaissent dans le résultat de
``lsblk``, ce qui permet de connaître précisément le nom à utiliser
pour l'accès à la clé USB. Notez bien que si la clé USB apparaît sur
plusieurs lignes, par exemple sous les noms *sdc*, *sdc1*, *sdc2*,
seul le nom sans numéro est à considérer (*sdX* est donc dans ce cas :
*sdc*).

Si c'est une image simple
-------------------------

La procédure de *gravure* est en tous points identique à la méthode pour
les images *lourdes* (voir ci-dessus).

Il y a cependant une différence : à la fin de la gravure sur une clé USB,
aucune partition n'est créée pour permettre de dédier une partie de la
clé à la *persistance des données*. On doit donc gérer ça séparément.

La persistance des données
--------------------------

Si on a juste gravé une image simple, la clé USB (ou le DVD-ROM) peut servir
à démarrer l'ordinateur, et le système GNU/Linux fonctionnera sans souci,
si ce n'est qu'au moment où on éteint l'ordinateur, toutes les données
qu'on a générées localement sont perdues, car celles-ci résident dans la
mémoire RAM de l'ordinateur, qui s'efface quand on l'éteint.

Ne **laisser aucune trace** peut avoir des avantages... Mais si on veut que
les données générées localement soient réutilisables au démarrage suivant,
comme on fait habituellement avec un ordinateur, il convient de mettre en 
place une partition servant à la persistance des données.

La méthode la plus simple est d'utiliser le logiciel ``live-clone``, sous
Linux. Celui-ci prend en charge la gravure de l'image « simple », et organise
automatiquement la mise en place d'une partition de persistance des données.

Créer une partition de persistance « à la main »
------------------------------------------------

Vous ne voulez pas utiliser le logiciel ``live-clone`` ? libre à vous !
ça va être un peu plus sportif ...

La présente explication est valable si on fait fonctionner un système
Linux (par exemple, en démarrant avec une clé gravée l'instant d'avant).

<ol>
  <li>
   <b>On repère bien le disque-cible</b> ; la clé USB où on fera une
   partition de persistance est considérée par Linux comme un disque. La
   commande en ligne <code>lsblk</code> permet d'avoir la liste des
   disques. Bien souvent, le disque-cible sera connu sous le nom
   <i>sdb</i>, et il aura une partition visible, sous le nom
   <i>sdb1</i>
  </li>
  <li>
   <b>On crée une nouvelle partition</b> ; on lance la commande
   <code>fdisk /dev/sdb</code> (à adapter autrement, si c'est plutôt
   <i>sdc</i> ou <i>sdd</i> qui est révélé par la commande
   <i>lsblk</i>). le programme <code>fdisk</code> est interactif
   ... en tapant <code>p</code> (Print) : fdisk vous présentera les
   partitions existantes, numérotées 1 et 2.
   <br>
   Sous le contrôle de <code>fdisk</code>, tapez <code>n</code> (New),
   puis <code>p</code> (Primary partition), puis <code>3</code>, et
   enfin plusieurs fois sur la touche Entrée.
   <br>
   Cela crée une troisième partition qui utilise tout l'espace
   disponible. Tapez <code>p</code> (Print), vous pourrez bien
   vérifier la structures, qui comporte trois partitions, numérotées
   1, 2 et 3. Si tout va bien, tapez <code>w</code> (Write) : ça écrit
   la nouvelle table de partition en écrasant l'ancienne. On peut
   abandonner en cas de doute, en tapant <code>q</code> (Quit) ; cela
   arrête fdisk sans altérer la table de partition.
  </li>
  <li>
    <b>On formate la nouvelle partition</b> ; quand on dispose de
    l'invite du terminal (qu'on n'est plus sous le contrôle de <i>fdisk</i>),
    on lance la commande <code>mkfs -t ext4 -L persistence /dev/sdb3</code> ;
    remarquez bien que le mot *persistence* est en anglais, pas avec
    l'orthographe française persistance, ce détail a son importance.
	<br>
	Il faut bien sûr adapter la recette, si le disque n'est pas <i>sdb</i>
	mais <i>sdc</i> ou <i>sdd</i>, etc.
  </li>
  <li>
    <b>On documente la nouvelle partition</b> ; un fichier de
    renseignement doit être placé sur la partition fraîchement
    formatée. Vous pouvez ouvrir un outil graphique de gestion de
    fichiers : celui-ci aura détecté une partition de disque nommée
    persistence, si ce n'est pas le cas, vous pouvez tenter de
    débrancher la clé et de la rebrancher après quelques secondes. Le
    gestionnaire de fichiers permet de voir cette partition et
    d'apprendre le point de montage de cette partition. Typiquement,
    le point de montage est quelque chose comme 
	<code>/media/user/persistence</code> (<i>user</i> peut être
	un autre nom, celui de d'utilisateur courant).
	<br>
    Créez un fichier texte pur quelque part dans votre espace de travail,
	avec le nom
    de fichier <code>persistence.conf</code>, qui contiendra une ligne de texte, et une seule : <code>/ union</code> ; vérifiez soigneusement le nom du fichier, basé
    sur l'orthographe anglaise, et l'espace entre le caractère <code>/</code> et le
    mot <code>union</code>. 
	<br>
	Enfin, en tant que super-utilisateur, copiez ce
    fichier à la bonne place :
  </li>
</ol>

    sudo cp persistence.conf /media/user/persistence
	
(bien sûr, adaptez la syntaxe si le point de montage est autre que
*/media/user/persistence*).

Et voilà, lors de son prochain démarrage, le système de la clé USB disposera
de la persistance des données.

Comment vérifier si la persistance fonctionne bien
-------------------------------------------------

On peut vérifier que la persistance fonctionne, en enregistrant un fichier,
puis en arrêtant et en redémarrant la clé USB vive qu'on utilise. Le fichier
enregistré doit toujours être là, à la place où on l'avait mis.

Cependant c'est un peu long et décevant d'arrêter/redémarrer. C'est plus 
rapide, si on affiche un aperçu sommaire des points de montage. On lance
la commande :

    mount | grep persis
	
... qui présente la liste de tous les points de montage étiquetés avec
des mots comme "persistence". Si on voit alors une ligne mentionnant la
partition de persistance, alors c'est qu'elle fonctionne comme prévu.

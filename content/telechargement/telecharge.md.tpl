title:  Download and use, with GNU/Linux or Windows
lang: en
slug:     telecharge_utilise
author:      Georges Khaznadar
date:        {{ date }}
category: Téléchargement



First step : downloading the compressed image
=============================================

The download step aims to get an image of a GNU/Linux system;
the size of such images can fit a DVD-ROM (4.7 GB) or a USB stick
(use a size of 16 GB, if possible).

A so-called "heavy" image will be uncompressed up to 16 GB, and fits only
USB sticks. The "simple" image can fit a DVD-ROM or a USB stick; however it
does not manage the *persistent data* issue immediately.


<table>
<tr style="border: none; background: rgba(255,255,255,0);">
<td style="vertical-align: top; border: none;">
<ul id="images-gz">
{% for dist in distros %}
<li class="onglet-date"
    data-name="{{ dist.name }}"
    data-dir="{{ dist.dir }}"
    data-size="{{ dist.isofile.size or dist.hddfile.size }}"
    data-stick="{{ dist.stickimage.name }}"
    data-isoimage="{{ dist.isofile.name or dist.hddfile.name }}"
    data-sum="{{ dist.sums.name }}"
    data-sign="{{ dist.signature.name }}"
    ></li>
{% endfor %}
</ul>
</td>
<td style="vertical-align: top; border: none;">
<div id="links" class="multicol quatre">
  <div class="titre-grid col1">
    "Heavy" image, for a 16 GB USB stick
  </div>
  <div class="titre-grid col2">
    "Simple" image, for a USB stick or a DVD-ROM
  </div>
  <div class="titre-grid col3">
    Control sums
  </div>
  <div class="titre-grid col4">
    Signature of the control sums
  </div>
  <div class="stick alink col1">
	  <div class="centerlink">
      <a href="#">
		  <img src="{attach}/img/download.png" title="heavy image" alt="link to the heavy image"/><br>
		  <span></span>
	  </a>
	  </div>
  </div>
  <div class="isoimage alink col2">
	  <div class="centerlink">
	    <a href="#">
		  <img src="{attach}/img/download.png" title="simple image" 
		      alt="link to the simple image" style="width: 120px;"/>
		  <img src="{attach}/img/download2.png" title="simple image" 
		      alt="link to the simple image" style="width: 120px;"/>
	      <br>
		  <span></span>
	   </a>
	</div>
  </div>
  <div class="sum alink col3">
	<div class="centerlink">
        <a href="#">
		  <img src="{attach}/img/checksums.png" title="control sums"
		       alt="link to the control sums"/>
	    </a>
	</div>
  </div>
  <div class="sign alink col4">
	<div class="centerlink">
      <a href="#">
        <img src="{attach}/img/signatures.png" title="signature" alt="link to the signature"/>
      </a>
	</div>
  </div>
</div>
</td>
</tr>
</table>

<script type="text/javascript" src="/theme/js/telecharge.js">
</script>

... OK, now it's downloaded ... But how can one trust the big file just
there? To know it, please go to the [certification](certif-en.html) page.

Second step: burning the image onto a medium
============================================

There are two different cases: "heavy" or "simple" image.

When it's a "heavy" image
-------------------------

There are two sub-steps:

* uncompress the file ``live-image-amd64.stick16G.img.gz``, to get a
  file named``live-image-amd64.stick16G.img``, weighing about 16 GB
* copy, or rather *burn* the file ``live-image-amd64.stick16G.img``
  onto the USB stick, whose size must be sufficient. Beware, if the stick
  has a bigger size, 32 GB for instance, only the first 16 GB will be
  consistently managed.

**Uncompressing** can be done with every good file manager, like
``file-roller`` under Linux, ``7zip`` under Windows, etc.

**Burning** is not equivalent to copying a file as usual:
the image does contain some data to write to the boot sector of the
medium.

To do it, one can use, under Windows, the free and cost-less software
[Win32 Disk Imager](https://sourceforge.net/projects/win32diskimager/files/Archive/win32diskimager-1.0.0-install.exe/download).

Under Linux, the utility ``gnome-disks`` (*package gnome-disk-utility*)
does the job when one wants to use the GUI, or one can also use
a command line:

    sudo dd if=<the_path_to>live-image-amd64.stick16G.img of=/dev/<sdX> bs=4M conv=fdatasync status=progress

... where *the_path_to* is the actual path to the image file
(can be empty if the image file is in the default directory when
the command is launched), and *sdX* represents the USB drive which
was plugged in to burn the image on it.

At any time, the command ``lsblk`` provides information about the list of
drives which might be accessed; when one plugs the USB drive in, some
additional lines will appear in ``lsblk``'s output, which discloses the
precise name to use to access the USB drive. Please notice than if the
USB drive appears on more than one line, for example with names
*sdc*, *sdc1*, *sdc2*, only the first name must be considered (so, in such
a case, *sdX* would be: *sdc*).

When it's a simple image
------------------------

The **burning** process is completely similar to the method used for
"heavy images" (see above).

However, there is a difference: when the image is burnt on a USB drive,
no partition is created to let use one part of the drive for
*data persistence*. That must be managed separately.


Data persistence
----------------

If one has burnt a simple image, the USB drive (or the DVD-ROM) can be used
to boot the computer, and the GNU/Linux system will run seamlessly. However
when one shuts down the computer, every data which were generated locally
are lost, because they live in the RAM, which is erased upon shutdown.

**Letting no track** can have some advantage... But if one wants to reuse
locally generated data after the next boot-up, as usual with computers,
one must create a partition for data persistence.

The easiest way is to use the ``live-clone`` program under GNU/Linux. That
program manages the burning process of a simple image, and then configures
automatically a partition for data persistence.

Creating a persistence partition "manually"
-------------------------------------------

You don't want to use ``live-clone``? you are free! there will just be
little more sports...

The present documentation is valid if one is running a Linux system
(for example, one can boot the USB drive just burnt a while ago).

<ol>
  <li>
   <b>Locate the target drive</b>; the USB drive where one wants to create
   a persistence partition is considered by Linux as a disk. The line-command
   program <code>lsblk</code> outputs the list of available drives. The target
   drive will often be known as <i>sdb</i>, and there will be a partition named
   <i>sdb1</i>
  </li>
  <li>
   <b>Create a new partition</b>; launch the command
   <code>fdisk /dev/sdb</code> (to adapt accordingly if it's rather
   <i>sdc</i> or <i>sdd</i> which is disclosed by the command
   <i>lsblk</i>). the program <code>fdisk</code> is interactive
   ... typing <code>p</code> (Print), fdisk will display existing
   partitions, numbered 1 and 2.
   <br>
   While <code>fdisk</code> is running, type <code>n</code> (New),
   and <code>p</code> (Primary partition), and then <code>3</code>, 
   and finally a few times on the Enter key.
   <br>
	This creates a third partition, which uses all the available space
	remaining on the drive. Type <code>p</code> (Print), to check the
	new structure, which should contain three partitions, numbered
    1, 2 and 3. If it wen well, type <code>w</code> (Write): the new
	partition table will overwrite the current one. It is possible to cancel
	if in doubt, by typing <code>q</code> (Quit); this halts fdisk without
	modifying the partition table.
  </li>
  <li>
    <b>Format the new partition</b>; when one is back to the shell prompt in
	the terminal (<i>fdisk</i> does no longer control the interface),
	launch the command <code>mkfs -t ext4 -L persistence /dev/sdb3</code>
	<br>
	Of course the recipe must be adapted when the drive is not <i>sdb</i>
	but <i>sdc</i> or <i>sdd</i>, etc.
  </li>
  <li>
    <b>Document the new partition</b>; one configuration file
	must be placed on the recently formatted partition.
	One can open a graphic file manager: it should detect a disk
	partition named "persistence". If this partition is not detected, one
	can try to unplug the drive, and plug it in after a short while.
	The file manager shows this partition, and discloses its mount point.
	Typically, the mount point is something like
	<code>/media/user/persistence</code> (<i>user</i> can be some other
	name, the name of the current user).
	<br>
	Create a plain text  file somewhere in your working area, which
	must be named <code>persistence.conf</code>, and contain exactly this only
	line: <code>/ union</code>
	<br>
	Be careful to insert a space between the character <code>/</code> and
	the word<code>union</code>. 
	<br>
	finally, as a <i>root</i> user, copy this file into the right location:
  </li>
</ol>

    sudo cp persistence.conf /media/user/persistence
	
(of course, adapt the command when the mount point is not
*/media/user/persistence*).

Et voilà! when it will booted, later, the system of the USB drive will
benefit from data persistence.

How to check whether the data persistence is running
----------------------------------------------------

One can check whether the data persistence is running, by saving a file,
then rebooting the drive. The saved file should be there, at the same place.

However it rather long and frustrating to reboot. It is faster to display
a short summary of mounting points. Launch this command:

    mount | grep persis
	
...  this will display the list of all mounting points labeled with
words like "persistence". If one sees a line mentioning the persistence
partition, then everything works as expected.

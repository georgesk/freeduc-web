title:       Installer plus d'applications
subtitle:    la logithèque Debian
lang: fr
slug:     logithèque
author:      Georges Khaznadar
date:        2025-01-17
category: Technique

# La logithèque

Les clés vives Freeduc sont basées sur la distribution
[Debian](https://www.debian.org), qui maintient à ce jour la logithèque la
plus riche : plus de soixante mille paquets.

## Combien de paquets logiciels avec Freeduc ?

La clé Freeduc vient avec quelques deux mille paquets logiciels, qui font
partie de la *zone GNU/Linux*. Ça donne, dans le menu graphique 
des applications, plus de cent entrées distinctes.

N'est-ce pas étonnant ? deux mille paquets logiciels, et seulement 
cent applications pour l'utilisateur... Ça peut paraître étonnant, cependant
c'est encore bien pire quand on regarde « à la loupe » l'organisation de la
logithèque d'un ordinateur sous Windows ou Mas OS.

Dans la clé Freeduc, les cent applications utilisateur sont soutenues par 
un nombre important de *bibliothèques logicielles*, c'est à dire des 
programmes informatiques qui sont utilisés et réutilisés par plus d'une
application. Une des caractéristiques-clé de la distribution Debian, sur
laquelle se fonde Freeduc, c'est d'éviter de multiplier le nombre de
bibliothèques logicielles. Quand deux programmes utilisateur ont les mêmes
besoins, ils utilisent « en coulisse » les mêmes bibliothèques.

## Ajout de nouveaux paquets logiciels

Ainsi, si on ajoute à la logithèque « standard » de Freeduc de nouvelles
applications, le nombre d'entrées dans les menus augmente, mais il y a
de moins en moins souvent besoin de télécharger les bibliothèques dont
dépendent les nouveaux paquets logiciels, car ces bibliothèques sont
déjà là. 

Si on considère la logithèque totale de Debian (personne n'a besoin
d'installer tout cela !), ça fait donc des milliers d'applications 
pour l'utilisateur, des bonnes, des excellentes, des moins bonnes, qu'on
est en mesure d'installer.

## Comment gérer les logiciels

### En mode graphique

En passant par le menu : **Administration -> Gestionnaire de paquets Synaptic**

Un mot de passe est demandé : on peut taper « live » (sans les guillemets)
pour répondre.

<img src="{attach}/img/architecture/synaptic1.png" alt="copie d'écran de Synaptic"/> 

<dl>
  <dt>Mettre à jour les catalogues</dt>
  <dd>
    La toute première fois, le gestionnaire de paquets ne montre que
	la logithèque déjà installée, il n'y a pas d'autres paquets à
	rechercher. Il faut cliquer sur le bouton en haut à gauche, pour
	« Recharger » les dictionnaires des paquets disponibles. On peut
	cliquer sur ce bouton quand l'accès à Internet est possible.
	<br>	
    Après ce <i>rechargement</i> on voit une majorité de paquets
	<i>non-installés</i>, à commencer par <tt>0ad</tt>,
	<tt>0ad-data</tt>, <tt>0ad-data-common</tt>, <tt>0install</tt>,
	etc. La petite case à gauche de chaque nom de paquet est un carré
	<i>blanc</i> quand le paquet n'est pas installé.
  </dd>
  <dt>Chercher un paquet</dt>
  <dd>
    Le plus simple, pour trouver ce qu'on cherche, c'est de visiter des
	forums sur Internet, si possible traitant des sujets appropriés. 
	<br>
	On peut recommander le site
	[framalibre.org](https://framalibre.org/), qui propose un moteur
	de recherche qui renvoie à des notices écrites par une foule de
	contributeurs qui décrivent chacune et chacun un logiciel libre
	intéressant. Par exemple, si on tape « Vidéo » dans le champ de
	recherche, on obtient quelques cent résultats : parmi les
	premiers, on trouve <b>Kdenlive</b>, qui donne accès à 
	[la notice](https://framalibre.org/notices/kdenlive.html), où on
	lit que ce <i>logiciel libre</i> est « Disponible sur GNU/Linux, BSD,
	Mac OS X, Windows » et que « Kdenlive est un logiciel de montage
	vidéo permettant la création de films amateurs et
	professionnels... »
  </dd>
  <dt>Installer un logiciel</dt>
  <dd>
	Le bouton de recherche, dans le gestionnaire de paquets Synaptic,
	permet de trouver le paquet <tt>kdenlive</tt> ; c'est bien plus
	facile de trouver dans Synaptic quand on connaît <i>déjà</i> le
	nom de paquet. 
	<br>
	Si on veut installer ce logiciel, on clique sur la
	case juste à gauche, puis on peut cliquer sur le bouton
	« Appliquer » de la barre de boutons de Synaptic. Il faudra
	probablement confirmer qu'on installe aussi quelques autres
	paquets dont dépend le paquet <tt>kdenlive</tt>, ne serait-ce que
	le paquet <tt>kdenlive-data</tt>. Un dialogue permet ensuite de
	suivre les étape : téléchargement, installation, configuration des
	paquets. À la fin de l'opération, Kdenlive se trouve dans les
	menus, sous la rubrique Son/Vidéo, prêt à l'usage.
  </dd>
</dl>

### En mode ligne de commande

La gestion des paquets logiciels est plus rapide, avec un peu de pratique,
quand on utilise une *ligne de commande*

<dl>
<dt>Lancer un Terminal</dt>
<dd>
  Dans les menus, on clique sur Administration -> Terminal
</dd>
<dt>Mettre à jour les catalogues</dt>
<dd>
  Si jamais les catalogues de paquets n'ont été téléchargés, il en va
  de même qu'en mode graphique : il faut bien commencer par leur
  chargement.  Pour cela on tape : <tt>sudo apt update</tt> et on
  valide. Le verbe <tt>sudo</tt> avant la commande <tt>apt</tt>
  signifie qu'on veut travailler en mode <i>administrateur</i> ; la
  commande <tt>apt</tt> (<i>a package tool</i> en anglais) accepte
  comme complémnt <tt>update</tt> afin de déclencher une mise à jour
  des catalogues de paquets.
  
  Notez bien que tous les mots (verbes, compléments) du langage de la ligne de
  commande sont séparés par des espaces, et qu'on termine toujours en appuyant
  sur la touche Entrée.
</dd>
<dt>Chercher un paquet</dt>
<dd>
  Une commande <tt>apt-cache search kdenlive</tt> permet de vérifier si le nom
  <i>kdenlive</i> est connu dans les catalogues.
  
  Une commande <tt>apt-cache show kdenlive</tt> permet de voir tout ce qui
  est connu au sujet du paquet kdenlive
  
  Notez bien qu'il est inutile de préfixer avec <tt>sudo</tt> : ces 
  consultattions n'ont pas besoin des permissions d'administrateur.
</dd>
<dt>Installer un logiciel</dt>
<dd>
  La commande <tt>apt install kdenlive</tt> permet d'installer (ou de 
  mettre à jour si une version plus récente est dans les catalogues)
  le paquet logiciel kdenlive
</dd>
</dl>

## Sécurité des paquets logiciels

À moins d'une modification des fichiers système de Freeduc, les catalogues
de paquets logiciels pointent uniquement vers des logiciels maintenus et 
certifiés par la communauté des développeurs Debian, dont la charte
interdit les logiciels dérangeants pour l'utilisateurs : évidemment tout
ce qui serait virus, malware, mais aussi et surtout tous les logiciels qui
laissent sur Internet des traces de l'utilisateur permettant de le profiler.

La cybersécurité est prise très au sérieux par les développeur Debian,
et c'est bien pourquoi de nombreux responsables choisissent Debian/GNU/Linux
pour leurs services web critiques.


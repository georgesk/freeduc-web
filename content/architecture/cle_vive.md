title:       Une clé vive, comment ça marche ?
subtitle:    version 2025
lang: fr
slug:     cle_vive
author:      Georges Khaznadar
date:        2025-01-17
category: Technique

# Pourquoi s'inquiéter de la technique ?

La plupart du temps, on utilise une clé vive, ça marche et ça suffit comme ça.

Cependant, ça vaut la peine d'en savoir un peu plus, pour plusieurs raisons.

## Ça peut donner des idées

Eh oui, pourquoi ne pas créer une nouvelle clé vive, encore mieux adaptée
aux besoins ? Un peu de technique, ça permet tout des suite de savoir ce
qui serait impossible... Du coup, tout le reste semble possible, y'a plus
qu'à !

## Si quelqu'un a abîmé la clé, on peut peut-être la réparer :)

Les clés vives diffusées sur le site 
[usb.freeduc.org](https://usb.freeduc.org) ont longtemps servi à 
des étudiants, qui avaient le droit de tout tenter, y compris des
actions susceptibles de « casser le système » si on fait une erreur.

Il suffit de taper <tt>sudo</tt> juste avant une *commande en ligne*
pour lancer une application avec des permissions d'administrateur. S'il
s'agit d'une commande en ligne effaçant des fichiers, on peut alors 
par inadvertance effacer des choses importantes pour la stabilité du
système.

Bonne nouvelle ! Dans le pire des cas, il y a une démarche simple pour
restaurer un fonctionnement normal de la clé vive, si elle est abîmée.

## Ça permet de comprendre pourquoi ça va si vite d'installer le système sur un disque dur

Une installation de GNU/Linux sur un disque dur d'ordinateur, ça se
pratique d'ordinaire durant une **install-partie** : on rassemble dans
une même pièce des personnes avec divers niveaux de compétence, et les
plus avancées montrent aux autres comment procéder à l'installation de 
GNU/Linux sur leurs ordinateurs.

Typiquement il faut entre une demi-heure et trois quarts d'heure pour
finir d'installer GNU/Linux pour un usage « ordinaire » : bureautique, 
Internet, éditeurs graphiques, quelques jeux.

**Freeduc** est installable en trois minutes seulement. Ça va extrêmement
vite, c'est un avantage : ça vaut la peine de voir si cet avantage a des
contreparties, un peu de connaissance technique permet d'y voir plus clair.

# Les deux parties principales d'une clé Freeduc

<img src="{attach}/img/architecture/live_usb.svg" alt="résumé : une clé = un démarreur + un système gnu/linux"/> 

Deux parties importantes constituent une clé Freeduc : le démarreur et 
le système lui-même.

## Le démarreur

Cette partie pèse un peu plus de deux cents méga-octets sur la clé.

Il s'agit d'un choix de programmes (<tt>grub</tt>, <tt>syslinux</tt> ou
<tt>isolinux</tt>), dont un sera activé au démarrage de l'ordinateur, si 
lui fait prendre en compte la clé vive. Le démarrage (le *boot*) dure 
quelques secondes, c'est une opération importante qui ouvre la voie au
déploiement d'un système GNU-Linux complet.

Grosso modo, si le démarreur a pu être activé (la clé vive a bien été prise
en compte), il interagit avec la carte mère de l'ordinateur et les
programmes minimaux qui y sont implantés : soit un système *UEFI*, soit
un système plus ancien, souvent désigné comme *legacy* (héritage).

Le démarreur affiche à l'écran un choix, qu'il suffit d'ignorer si on
a dix secondes de patience, ou qu'on peut laisser de côté en appuyant
sur la touche *Entrée*. Les autres possibilités permettent de passer
en « mode expert » et on n'en a pas besoin la plupart du temps.

Tout de suite après cet écran d'accueil, le démarreur passe la main à un
noyau GNU/Linux, qui prend le contrôle de l'ordinateur de façon plus fine
et plus puissante que le simple démarreur. Les étapes suivantes sont :

 - vérification de l'intégrité de l'ordinateur, accès à la mémoire, aux
   disques, à toutes les parties de la clé vive, etc.
 - basculement en mode graphique de la carte d'écran, et éventuellement
   affichage d'une image animée pour attendre une minute.
 - mise en mémoire et activation des modules du noyau. On peut noter que
   le noyau chargé au départ est dans la *zone du démarreur*, cependant
   que les modules sont dans l'autre, c'est à dire la *zone « GNU/Linux »*
 - séquence de démarrage des logiciels d'arrière plan, qui fonctionneront
   « en coulisse » au service de l'utilisateur
 - démarrage de l'environnement de bureau :l'image animée disparaît 
   quelques secondes, c'est le *bureau* qui est présenté ensuite.
   
## GNU/linux

Quand on voit à l'écran l'image animée d'attente, c'est que le démarreur 
a déjà passé le contrôle au système GNU/Linux.

Physiquement, le système d'exploitation GNU/Linux ainsi que toutes les
applications livrées « en standard » avec la clé vive sont dans un seul
fichier de nom <tt>filesystem.squashfs</tt> fortement comprimé,
qui pèse environ trois giga-octets : le
démarreur ne représente que cinq à dix pour cent de ce fichier-là.

Le noyau Linux est équipé d'algorithmes qui savent décompresser
*à la volée*, c'est à dire au besoin et pas plus qu'il ne faut, les données
que contient le gros fichier <tt>filesystem.squashfs</tt>. Ses trois
giga-octets compressés représentent dix à quinze giga-octets de données,
après décompression.

Un des points importants à retenir, c'est que la *zone GNU/Linux* de la clé
vive est en **lecture seule**, c'est à dire qu'en fonctionnement normal,
rien n'est modifié dans ce gros fichier-là. Bien sûr, il y a des
modifications, ne serait-ce que tout de suite, avec la journalisation des
évènements du démarrage. Cependant, ces modifications se font dans une
zone séparée, nommée *zone de persistance*. C'est là l'origine d'un truc
magique qu'on détaillera un peu plus loin.

Quand l'environnement de bureau est actif, il devient possible d'accéder
aux applications :

<dl>
<dt>En mode graphique</dt>
<dd>
  C'est le mode le plus utilisé, depuis que la micro-informatique
  s'est largement diffusée : on déplie le menu (en bas à gauche) et on
  sélectionne parmi les catégories, une application qu'on lance d'un
  clic de souris. Le menu permet aussi de rechercher une application en
  tapant quelques mots-clés.
</dd>
<dt>En mode ligne de commande</dt>
<dd>
  Ça vaut la peine de mentionner le mode *en ligne de commande*, plus ancien
  que le mode graphique. Contrairement à ce qui se pratique avec des systèmes
  d'exploitation dominants du marché, qui exploitent l'inculture des
  utilisateurs, les système GNU/Linux n'occultent pas ce mode, qui est
  puissant quand on a appris à le maîtriser.
</dd>
</dl>


# La persistance : c'est magique ?

Si vous savez compter, vous avez compris qu'en additionnant les poids du
*démarreur* et de la *zone  GNU/Linux*, on n'arrive pas à remplir beaucoup
une clé USB de 16 giga-octets, comme on en trouve pour dix euros sur le
marché. Il reste une zone de quinze méga-octets.

Les clés vives Freeduc, quand elles ont été bien réalisées, disposent de
cette place pour organiser une troisième zone, la *zone de persistance*.

<img src="{attach}/img/architecture/live_usb2.svg" alt="résumé : une clé = un démarreur + un système gnu/linux + la persistance" style="margin: 1em;"/> 

Cette *zone de persistance* est organisée en un système de fichiers, qui est
monté « en surplomb », par-dessus le système GNU/Linux fourni par le fichier
<tt>filesystem.squashfs</tt>, qui lui, reste en *lecture seule*.

## Magie du montage « en surplomb » (*overlay mount* en anglais)

Très tôt dans la séquence de démarrage, la *zone de persistance*, si elle est
définie, est montée « en surplomb ». On peut imaginer ça comme un système de
fichiers placé sur un papier calque, ou mieux, sur un plancher transparent.

Quand GNU-Linux a besoin de trouver un fichier, par exemple le fichier 
<tt>/etc/apt/preferences</tt>, le système « regarde » cette espèce de
plancher transparent : 

 - si le fichier <tt>/etc/apt/preferences</tt> est
   enregistré sur ce plancher (autrement dit dans la zone de persistance), 
   c'est lui qui sera pris en compte, il peut être lu et écrit (modifié,
   effacé, etc.)
 - sinon, le système regarde en-dessous à travers le plancher transparent,
   vers le GNU/Linux en lecture seule : si le fichier de l'exemple,
   <tt>/etc/apt/preferences</tt> s'y trouve, c'est celui-là qui sera
   considéré *en lecture*
 - dans tous les cas, si le système doit écrire dans le fichier 
   <tt>/etc/apt/preferences</tt>, l'écriture a lieu sur le calque ou 
   plancher transparent : en aucun cas la deuxième zone de la clé vive,
   qui est en lecture seule, n'est modifiée. C'est pareil si le système veut
   effacer le fichier <tt>/etc/apt/preferences</tt> : il dépose alors une
   sorte de masque afin de ne plus jamais voir ce même fichier s'il était
   présent dans la zone en lecture seule.
   
## Conséquence de la magie (de la mise « en surplomb »)

Comme la deuxième zone de la clé USB, le fichier <tt>filesystem.squashfs</tt>
reste en lecture seule, si l'utilisateur de la clé fait des manœuvres
délétères en mode administrateur et rend la clé inutilisable, cette clé,
même abîmée, contient quand même toutes les données qui permettraient un
fonctionnement correct. 

À l'aide d'une deuxième clé (non abîmée), on peut utiliser un outil de
réparations, pour agir sur la clé abîmée. Cet outil fonctionne 
en deux temps :

 1. Copie des données qu'on souhaite conserver, dans des *archives*. Comme
    la deuxième étape effacera la zone de persistance, c'est une bonne
	idée, afin de pouvoir retrouver ensuite tous les travaux
	personnels à ne pas perdre ;
 2. Remise à blanc de la zone de persistance : la clé vive abîmée redevient
    comme une clé Freeduc neuve, fonctionnelle. Toutes les causes de
	non-fonctionnement disparaissent.
	
Ensuite, si les *archives* contiennent bien les données à récupérer, il suffit
de les rouvrir, puis remettre en place les travaux personnels.

title:       Le paquet Kwartz-client
subtitle:    Méthode vérifiée au lycée Jean Bart
lang: fr
slug:     kwartz-client
author:      Georges Khaznadar
date:        2019-10-20
category: Kwartz



# Adapter une station Debian à un serveur Kwartz #
Dans de nombreux collèges et lycées, un service LDAP est fourni par un serveur
Kwartz (voir  https://www.kwartz.com). D'où le nom de paquet,
<tt>kwartz-client</tt>. On peut cependant utiliser ce paquet pour tout autre
annuaire LDAP, à condition de comprendre les questions posées durant
la configuration du paquet.

## Le paquet kwartz-client ##

À la date où cet article est publié, le paquet <tt>kwartz-client</tt>
est dans l'archive officielle Debian, dans la prochaine distribution
stable, *bullseye*. On peut accéder à la description du paquet, puis
le télécharger en visitant https://packages.debian.org/unstable/kwartz-client

Les dépendances du paquet <tt>kwartz-client</tt> sont les suivantes :
<ul>
<li>libpam-ldapd,</li>
<li>libnss-ldapd,</li>
<li>libpam-modules,</li>
<li>cifs-utils,</li>
<li>smbclient,</li>
<li>winbind,</li>
<li>libpam-mount-bin,</li>
<li>oidentd,</li>
<li>inetutils-inetd,</li>
<li>hxtools</li>
</ul>
Toutes ces dépendances peuvent être trouvées dans la distribution 
Debian 10 (nom de code <tt>buster</tt>).


Pour la suite, on suppose qu'une distribution Debian 10 est installée et active
sur l'ordinateur que l'on configure, et qu'on dispose des droits 
d'administration. L'invite `# ` dans les lignes de commande rappelle que
les commandes ci-dessous sont validées par le super-utilisateur de la machine
(root).

## Étapes de l'installation de kwartz-client ##

Comme le paquet kwartz-client n'est pas actuellement intégré dans la
distribution Debian 10, mais que ses dépendances y sont, on commence par
télécharger directement l'archive du paquet, par exemple, si la version est 
1.0-4, le paquet est téléchargeable par exemple depuis
http://ftp.fr.debian.org/debian/pool/main/k/kwartz-client/kwartz-client_1.0-4_all.deb

### installation (incomplète) du paquet kwartz-client ###

Mettons que le paquet <tt>kwartz-client_1.0-4_all.deb</tt> soit téléchargé ;
alors, on l'installe par la commande :
`# dpkg -i kwartz-client_1.0-4_all.deb`

L'archive du paquet sera dépliée, mais très probablement le paquet ne sera
pas installé : il suffit qu'une dépendances soit manquante pour interrompre
l'installation.

### installation des dépendances du paquet kwartz-client ###
Le mieux est alors de faire rechercher les paquets manquants de façon
automatique par la commande `apt`. Cela suppose
un accès à Internet. Si on est `root` sur la machine, on peut déclarer
le proxy de la façon suivante :

`# export http_proxy=<adresse IP du serveur>:3128`

Quand le proxy est correctement défini, on lance la commande :

`# apt update`

pour mettre à jour les catalogues de paquets de logiciels ; si ça ne
marche pas, configurer le service KWARTZ afin qu'il donne accès à Internet
sans filtrage, pour la machine qu'on configure. Une autre façon de faire
consiste à installer le paquet `oidentd`, mais on entre alors dans un problème
de poule et d'œuf.

Quand on utilise l'outil `apt`, celui-ci ajoute automatiquement les
dépendances par rapport aux paquets demandés. Si la commande `apt update`
a réussi, alors il suffit de lancer la nouvelle commande `apt -f install`,
qui va repérer toute inconsistance dans les paquets installés et chercher
à réparer les inconsistances. La conséquence de la commande
`apt -f install` est de télécharger, installer, configurer les dépendances
de <tt>kwartz-client</tt>. Des questions seront posées durant les 
configurations des dépendances ... ce n'est pas gênant si on se trompe
à ce moment-là dans les réponses, car ... *soyez attentif* ... au moment
où commencera la configuration de <tt>kwartz-client</tt>, toutes les
questions qui ont de l'importance seront posées à nouveau, et là il sera
important de donner les bonnes réponses.

## Le dialogue de configuration de kwartz-client ##

Avant de commencer à configurer <tt>kwartz-client</tt>, il convient
de préparer un petit formulaire papier, et de le remplir avec l'aide
du responsable réseau. Voici le formulaire :

> **Adresse IP du service LDAP** : ..........................................<br/>
> cette adresse peut être quelque chose comme 172.16.0.254, mais on peut aussi
> accepter un nom symbolique comme serveur.lycee.jb par exemple
>
> **Port du service LDAP s'il n'est pas standard** : ........................<br/>
> Le numéro de port standard est 389 ; il existe quelques cas où l'accès
> au service LDAP se fait par un port différent, par exemple 1389.
>
> **La base à partir de laquelle chercher dans l'annuaire** : ...............<br/>
> Il s'agit le plus souvent de la fin de l'adresse IP symbolique du
> serveur Kwartz, autrement dit, de deux mots qui en définissent le domaine
> Internet. Si l'adresse IP est serveur.lycee.jb, alors la base est :<br/>
> dc=lycee,dc=jb
>
> **L'accès à un utilisateur de la base de données** : ......................<br/>
> Depuis l'année 2018, il n'est plus possible de consulter l'annuaire de façon
> totalement anonyme. Par conséquent, il faut disposer d'un nom d'utilisateur.
> Conseil pour l'administrateur du réseau : le mieux est de définir un
> utilisateur dans un groupe sans importance, mais dont on est sûr qu'il ne
> sera pas effacé par une opération de maintenance plus tard. Donc s'il y
> a des groupes d'élèves, de professeurs, ou de membres du personnel, éviter
> ces groupes où on est amené à effacer des comptes.
> Voici un exemple, où un utilisateur de login "personne" a été défini :<br/>
> cn=personne,cn=Users,dc=lycee,dc=jb
>
> **Le mot de passe de cet utilisateur_là** : ...............................<br/>
> Chaque utilisateur de l'annuaire a un mot de passe. Pour l'utilisateur
> sans pouvoir défini ci-dessus, il faudra marquer le mot de passe (ne
> pas utiliser de mot de passe totalement trivial).
>
> **Le nom NETBIOS du serveur Kwartz** : ....................................<br/>
> Il s'agit de la façon dont le serveur Kwartz apparaît dans le réseau des
> machines Windows. Exemple :<br/>
> SERVEUR

Quand le formulaire papier est rempli, on doit préparer l'URI LDAP du serveur.
<dl>
<dt>Syntaxe simple</dt>
<dd>Le numéro de port est standard (389) ; l'URI LDAP est :<br/>
ldap://&lt;adresse IP du serveur>/</dd>
<dt>Syntaxe complète</dt>
<dd>Le numéro de port n'est pas standard ; l'URI LDAP est :<br/>
ldap://&lt;adresse IP du serveur>:&lt;numéro de port>/</dd>
</dl>

Et voilà, vous êtes prêt à configurer le paquet. Lancez la commande
`sudo dpkg-reconfigure kwartz-client` à chaque fois où vous souhaitez
rejouer le dialogue de configuration de kwartz-client.

# J'ai installé kwartz-client, et ensuite ? #

Si la configuration de <tt>kwartz-client</tt> est bien faite, et que 
l'ordinateur est bien dans le réseau local où les services du Kwartz
sont disponibles, alors on doit pouvoir *changer d'utilisateur*, et
s'authentifier sur le réseau Kwartz : le plus souvent avec un login du type
nom.prenom, et le mot de passe qui y est associé.

Si la configuration est erronée, alors l'authentification ne fonctionne pas
bien. On peut parcourir à nouveau chaque étape du dialogue de configuration,
par la commande `# dpkg-reconfigure kwartz-client`

Quand l'authentification se passe bien, l'utilisateur authentifié reçoit
un bureau graphique, sur lequel apparaîtront trois montages réseaux :
<dl>
<dt>Perso</dt>
<dd>pour l'accès aux données personnelles, dont le dossier **Travail**,
que Windows interprète parfois comme un dossier « Mes Documents »,
dans le lecteur-réseau nommé <tt>H:</tt> ;</dd>
<dt>Commun</dt>
<dd>pour l'accès aux données « communes », relatives à chaque groupe
d'appartenance de l'utilisateur. Les données communes sont en
lecture/écriture ;</dd>
<dt>Public</dt>
<dd>pour l'accès aux données « publiques », relatives à chaque groupe
d'appartenance de l'utilisateur. Les données publiques sont en
lecture seule pour les élèves, en lecture/écriture pour les
professeurs.</dd>
</dl>

# Le paquet kwartz-client ajoute quelques applications #
## kwartz-cloud ##
<img src="/img/kwartz-cloud.png" alt="logo de kwartz-cloud" style="float: left; padding-right: 1em;"/>
Un serveur Kwartz offre un service owncloud, qui permet l'accès aux données
personnelles maintenues sur le serveur à travers Internet. Ce service est
fort commode pour travailler à distance, depuis chez soi par exemple, tout
en ayant accès aux données « du lycée ».

L'application se trouve dans les accessoires du menu, avec le logo ci-contre.
Quand l'application est lancée, on s'authentifie, puis on a l'accès directement
dans le gestionnaire de fichiers.

## kwartz-proxy ##
`kwartz-proxy.service`  est un service lancé une fois, au boot de la clé 
Freeduc. Son bon fonctionnement dépend du bon réglage du paquet `kwartz-client`.
Si les réglages sont corrects, une recherche est faite pour découvrir le
service de proxy Internet du serveur Kwartz. Si ce service est découvert, alors
des fichiers de configuration sont mis à jour, afin de faciliter certaines
commandes Linux, telles que `apt`pour la gestion des paquets, ou `wget` pour
les téléchargements.

# Licence de ce document ![Logo CC-BY-SA 4.0](img/cc-by-sa88x31.png) #

Vous êtes libre de copier ce document, de le distribuer tel quel ou
modifié ; la licence CC-BY-SA vous oblige cependant à citer le ou les
auteurs, et à transmettre le texte de cette même licence avec le
document, qu'il soit ou non modifié.

L'énoncé complet de la licence **Creative Commons - By - Share Alike**
peut être téléchargé depuis 
[https://creativecommons.org/licenses/by-sa/4.0/legalcode](https://creativecommons.org/licenses/by-sa/4.0/legalcode)

title:       Adapter une station Debian à un serveur Kwartz
subtitle:    Méthode vérifiée au lycée Jean Bart
lang: fr
slug:     kwartz4all
author:      Georges Khaznadar
date:        2019-02-13
category: Kwartz



# Adapter une station Debian à un serveur Kwartz #

## Installation des paquets nécessaires à l'authentification par l'annuaire ##

Pour la suite, on suppose qu'une distribution Debian est installée et active
sur l'ordinateur que l'on configure, et qu'on dispose des droits 
d'administration. L'invite `# ` dans les lignes de commande rappelle que
les commandes ci-dessous sont validées par le super-utilisateur de la machine
(root).

Les paquets sont rapatriés à l'aide de la commande `apt`. Cela suppose
un accès à Internet. Si on est `root` sur la machine, on peut déclarer
le proxy de la façon suivante :

`# export http_proxy=<adresse IP du serveur>:3128`

Quand le proxy est correctement défini, on lance la commande :

`# apt update`

pour mettre à jour les catalogues de paquets de logiciels ; si ça ne
marche pas, configurer le service KWARTZ afin qu'il donne accès à Internet
sans filtrage, pour la machine qu'on configure. Une autre façon de faire
consiste à installer le paquet `oidentd`, mais on entre alors dans un problème
de poule et d'œuf.

Quand on utilise l'outil `apt`, celui-ci ajoute automatiquement les
dépendances par rapport aux paquets demandés. Par exemple, le paquet
`libpam-ldapd` **dépend** du paquet `nlscd`. Donc, si on demande à
installer le premier paquet, l'outil `apt` ajoute automatiquement le
deuxième qui est une dépendance.

### `libpam-ldapd` et sa configuration ###

Au lycée Jean Bart, la base LDAP est `dc=lycee,dc=jb` et l'adresse IP
du serveur est `serveur.lycee.jb`.

Il suffit de valider (ce qui revient à sélectionner `<OK>`),
après avoir vérifié que les options retenues sont correctes et
adaptées au réseau KWARTZ.

<dl>
  <dt>installation du paquet</dt>
  <dd> <code># sudo apt install libpam-ldapd</code> </dd>
  <dt>Configuration du paquet</dt>
  <dd><pre>
 ┌────────────────────────┤ Configuration de nslcd ├─────────────────────────┐
 │ Veuillez indiquer l'URI (« Uniform Resource Identifier ») du serveur      │ 
 │ LDAP à utiliser. Il s'agit d'une adresse de la forme « ldap://&lt;nom de     │ 
 │ machine ou IP&gt;:&lt;port&gt;/ ». Des adresses sous la forme « ldaps:// » et      │ 
 │ « ldapi:// » peuvent aussi être utilisées. Le numéro de port est          │ 
 │ facultatif.                                                               │ 
 │                                                                           │ 
 │ Lorsque le protocole utilisé est « ldap » ou « ldaps », il est            │ 
 │ recommandé d'utiliser une adresse IP plutôt qu'un nom d'hôte afin de      │ 
 │ réduire les risques d'échec en cas d'indisponibilité du service de noms.  │ 
 │                                                                           │ 
 │ Des adresses multiples peuvent être indiquées, séparées par des espaces.  │ 
 │                                                                           │ 
 │ URI du serveur LDAP :                                                     │ 
 │                                                                           │ 
 │ ldap://serveur.lycee.jb/_________________________________________________ │ 
 │                                                                           │ 
 │                    &lt;Ok&gt;                        &lt;Annuler&gt;                  │ 
 │                                                                           │ 
 └───────────────────────────────────────────────────────────────────────────┘ 
 ┌────────────────────────┤ Configuration de nslcd ├─────────────────────────┐
 │ Veuillez indiquer le nom distinctif (« DN ») de la base de recherche du   │ 
 │ serveur LDAP. Beaucoup de sites utilisent les éléments composant leur     │ 
 │ nom de domaine à cette fin. Par exemple, le domaine « example.net »       │ 
 │ utiliserait « dc=example,dc=net ».                                        │ 
 │                                                                           │ 
 │ Base de recherche du serveur LDAP :                                       │ 
 │                                                                           │ 
 │ dc=lycee,dc=jb___________________________________________________________ │ 
 │                                                                           │ 
 │                    &lt;Ok&gt;                        &lt;Annuler&gt;                  │ 
 │                                                                           │ 
 └───────────────────────────────────────────────────────────────────────────┘ 
</pre>

  </dd>
</dl>

### On doit cependant reconfigurer la dépendance `nslcd` ###

La configuration de « haut niveau » de `nslcd` ne règle pas tous
les problèmes liés au service KWARTZ.

Depuis l'année 2018, il est nécessaire de s'authentifier pour pouvoir
lire des enregistrements LDAP dans l'annuaire. Un utilisateur anonyme
**personne** a été défini, et son mot de passe est utilisé dans la
configuration de nslcd, ce qui permet au service de nslcd d'accéder aux
renseignements voulus dans l'annuaire. La création de cet utilisateur
se fait bien sûr dans l'outil Web KWARTZ~Control. Notez bien l'identifiant
et le mot de passe pour répondre aux questions de configuration.

Quand une copie d'écran suppose un choix, le bon choix est entouré
avec des étoiles, comme ceci : **\*le bon choix\***.

Quand il suffit de valider (ce qui revient à sélectionner `<OK>`),
rien n'est entouré avec des étoiles.

<dl>
  <dt>On lance la reconfiguration du paquet, qui est une dépendance pour libpam-ldapd</dt>
  <dd><code># dpkg-reconfigure nslcd</code></dd>
  <dt>Copies d'écran des dialogues...</dt>
  <dd>
<pre>
 ┌────────────────────────┤ Configuration de nslcd ├─────────────────────────┐
 │ Veuillez indiquer l'URI (« Uniform Resource Identifier ») du serveur      │ 
 │ LDAP à utiliser. Il s'agit d'une adresse de la forme « ldap://&lt;nom de     │ 
 │ machine ou IP&gt;:&lt;port&gt;/ ». Des adresses sous la forme « ldaps:// » et      │ 
 │ « ldapi:// » peuvent aussi être utilisées. Le numéro de port est          │ 
 │ facultatif.                                                               │ 
 │                                                                           │ 
 │ Lorsque le protocole utilisé est « ldap » ou « ldaps », il est            │ 
 │ recommandé d'utiliser une adresse IP plutôt qu'un nom d'hôte afin de      │ 
 │ réduire les risques d'échec en cas d'indisponibilité du service de noms.  │ 
 │                                                                           │ 
 │ Des adresses multiples peuvent être indiquées, séparées par des espaces.  │ 
 │                                                                           │ 
 │ URI du serveur LDAP :                                                     │ 
 │                                                                           │ 
 │ ldap://serveur.lycee.jb/_________________________________________________ │ 
 │                                                                           │ 
 │                    &lt;Ok&gt;                        &lt;Annuler&gt;                  │ 
 │                                                                           │ 
 └───────────────────────────────────────────────────────────────────────────┘ 

 ┌────────────────────────┤ Configuration de nslcd ├─────────────────────────┐
 │ Veuillez indiquer le nom distinctif (« DN ») de la base de recherche du   │ 
 │ serveur LDAP. Beaucoup de sites utilisent les éléments composant leur     │ 
 │ nom de domaine à cette fin. Par exemple, le domaine « example.net »       │ 
 │ utiliserait « dc=example,dc=net ».                                        │ 
 │                                                                           │ 
 │ Base de recherche du serveur LDAP :                                       │ 
 │                                                                           │ 
 │ dc=lycee,dc=jb___________________________________________________________ │ 
 │                                                                           │ 
 │                    &lt;Ok&gt;                        &lt;Annuler&gt;                  │ 
 │                                                                           │ 
 └───────────────────────────────────────────────────────────────────────────┘ 

 ┌────────────────────────┤ Configuration de nslcd ├─────────────────────────┐
 │ Veuillez choisir le type d'authentification que la base LDAP utilise (si  │ 
 │ nécessaire).                                                              │ 
 │                                                                           │ 
 │  - aucune : pas d'authentification;                                       │ 
 │  - simple : authentification simple avec un identifiant (DN) et un        │ 
 │             mot de passe;                                                 │ 
 │  - SASL   : mécanisme basé sur SASL (« Simple Authentication and          │ 
 │             Security Layer ») : méthode simplifiée d'authentification     │ 
 │             et de sécurité;                                               │ 
 │                                                                           │ 
 │ Authentification LDAP :                                                   │ 
 │                                                                           │ 
 │                                  aucune                                   │ 
 │                                 *simple*                                  │ 
 │                                  SASL                                     │ 
 │                                                                           │ 
 │                                                                           │ 
 │                    &lt;Ok&gt;                        &lt;Annuler&gt;                  │ 
 │                                                                           │ 
 └───────────────────────────────────────────────────────────────────────────┘ 

  ┌────────────────────────┤ Configuration de nslcd ├────────────────────────┐
  │ Veuillez indiquer le compte à utiliser pour s'identifier sur la base     │ 
  │ LDAP. Cette valeur doit être indiquée sur la forme d'un nom distinctif   │ 
  │ (DN : « Distinguished Name »).                                           │ 
  │                                                                          │ 
  │ Utilisateur de la base LDAP :                                            │ 
  │                                                                          │ 
  │ cn=personne,cn=Users,dc=lycee,dc=jb_____________________________________ │ 
  │                                                                          │ 
  │                   &lt;Ok&gt;                       &lt;Annuler&gt;                   │ 
  │                                                                          │ 
  └──────────────────────────────────────────────────────────────────────────┘ 

  ┌───────────────────────┤ Configuration de nslcd ├────────────────────────┐
  │ Veuillez indiquer le mot de passe à utiliser pour s'identifier sur la   │ 
  │ base LDAP.                                                              │ 
  │                                                                         │ 
  │ Mot de passe de l'utilisateur LDAP :                                    │ 
  │                                                                         │ 
  │ ********_______________________________________________________________ │ 
  │                                                                         │ 
  │                   &lt;Ok&gt;                       &lt;Annuler&gt;                  │ 
  │                                                                         │ 
  └─────────────────────────────────────────────────────────────────────────┘ 
 
 ┌────────────────────────┤ Configuration de nslcd ├─────────────────────────┐
 │                                                                           │ 
 │ Veuillez choisir si la connexion au serveur LDAP doit être chiffrée avec  │ 
 │ StartTLS.                                                                 │ 
 │                                                                           │ 
 │ Faut-il utiliser StartTLS ?                                               │ 
 │                                                                           │ 
 │                   *&lt;Oui&gt;*                      &lt;Non&gt;                      │ 
 │                                                                           │ 
 └───────────────────────────────────────────────────────────────────────────┘ 
                                                                               
   ┌─────────────────────┤ Configuration de nslcd ├─────────────────────┐
   │ En cas de connexion chiffrée, le certificat du serveur peut être   │ 
   │ demandé et contrôlé. Veuillez choisir la façon de réaliser ce      │ 
   │ contrôle :                                                         │ 
   │                                                                    │ 
   │  - Jamais    : certificat non demandé ni contrôlé ;                │ 
   │  - Autoriser : certificat demandé mais facultatif et non           │ 
   │                contrôlé ;                                          │ 
   │  - Essayer   : certificat demandé et contrôlé, mais facultatif ;   │ 
   │  - Demander  : certificat obligatoire et contrôlé.                 │ 
   │                                                                    │ 
   │ Contrôle du certificat SSL du serveur :                            │ 
   │                                                                    │ 
   │                            *Jamais*                                │ 
   │                             Autoriser                              │ 
   │                             Essayer                                │ 
   │                             Demander                               │ 
   │                                                                    │ 
   │                                                                    │ 
   │                 &lt;Ok&gt;                     &lt;Annuler&gt;                 │ 
   │                                                                    │ 
   └────────────────────────────────────────────────────────────────────┘ 
</pre>

  </dd>
  <dt>Modification du fichier <code>/etc/nslcd.conf</code></dt>
  <dd>
    Depuis l'année 2018, le schéma de l'annuaire de KWARTZ a changé, ces
	changements doivent se refléter dans la configuration du service nslcd.
	<br/>
	Il convient donc de s'assurer que les lignes suivantes font bien partie
	de ce fichier et qu'elles sont actives (non commentées) ; on peut les
	ajouter manuellement à la fin du fichier :
	
<pre>
map passwd uid cn
map passwd homeDirectory unixHomeDirectory

base passwd CN=Users,DC=lycee,DC=jb
base group  CN=Users,DC=lycee,DC=jb
</pre>

	en effet, le service LDAP n'utilise pas 'uid' pour les noms d'utilisateurs
	mais 'cn' à la place. Même chose, pour 'homeDirectory' et
	'unixHomeDirectory'. Enfin, redéfinir base est important pour le MAP group
	car sinon la recherche de groupe peut planter la connection LDAP.
  </dd>
</dl>


### `libnss-ldapd` et sa configuration ###

<dl>
  <dt>installation du paquet</dt>
  <dd> <code># apt install libnss-ldapd </code></dd>
  <dt>configuration du paquet</dt>
  <dd>
<pre>
  ┌────────────────────┤ Configuration de libnss-ldapd ├────────────────────┐
  │ Le fichier /etc/nsswitch.conf doit être modifié (afin d'utiliser la     │ 
  │ source de données « ldap ») pour rendre ce paquet fonctionnel.          │ 
  │                                                                         │ 
  │ Vous pouvez aussi choisir les services qui doivent être activés ou      │ 
  │ désactivés pour les requêtes LDAP. Les nouvelles requêtes LDAP seront   │ 
  │ ajoutées comme dernière source possible. Il est important de bien       │ 
  │ contrôler ces modifications.                                            │ 
  │                                                                         │ 
  │ Services de nom à configurer :                                          │ 
  │                                                                         │ 
  │    [*] passwd                                                       ↑   │ 
  │    [*] group                                                        ▮   │ 
  │    [*] shadow                                                       ▒   │ 
  │    [ ] hosts                                                        ↓   │ 
  │                                                                         │ 
  │                                                                         │ 
  │                                 &lt;Ok&gt;                                    │ 
  │                                                                         │ 
  └─────────────────────────────────────────────────────────────────────────┘ 
</pre>
  </dd>
</dl>

Une fois que `libnss-ldap` est configuré, on doit pouvoir accéder
à certaines données de l'annuaire :

<dl>
  <dt>Test d'un accès à l'annuaire</dt>
  <dd>
    <code># getent passwd &lt;login d'un utilisateur kwartz&gt;</code>
	<br/>
	Cette commande doit fournir des renseignement relatifs à l'utilisateur
	désigné. Notez bien que cette commande ne donne pas accès au mot de
	passe de l'utilisateur.
  </dd>
</dl>

## Paquets « de confort » pour gérer les répertoires ##

### Installation de modules pour PAM ###
<dl>
  <dt>Installation de libpam-modules</dt>
  <dd>`# apt install libpam-modules`</dd>
  <dt>Activation de la gestion automatique des répertoires</dt>
  <dd>
    On doit ajouter un ligne au sujet de `pam_mkhomedir.so` dans le fichier
	`/etc/pam.d/common-session`.
	<br/>
	Vérifier que toutes les lignes suivantes sont actives dans le fichier
	`/etc/pam.d/common-session` :
<pre>
session	[default=1]			pam_permit.so
session	requisite			pam_deny.so
session	required			pam_permit.so
session	required	pam_unix.so 
session	optional	pam_mount.so 
session	[success=ok default=ignore]	pam_ldap.so minimum_uid=1000
session	optional	pam_systemd.so 
session required pam_mkhomedir.so
</pre>

  </dd>
  <dt>Le paquet `hxtools` est nécessaire pour avoir la commande `ofl` :</dt>
  <dd>`# apt install hxtools`</dd>
  <dt>
    On doit bricoler des scripts invoqués par le module pam\_mkhomedir.
  </dt>
  <dd>
  <code>/usr/sbin/smbumount</code> et <code>/usr/sbin/smbmount</code> ont disparu
  de la distribution, mais le module pam\_mkhomedir les invoque encore.
  <br/>
  Contenu du fichier `/usr/sbin/smbumount` :
<pre>
&#35;! /bin/sh
umount -t cifs $@
true
</pre>
<br/>
  Contenu du fichier `/usr/sbin/smbmount` :
<pre>
&#35;! /bin/sh
mount -t cifs $@
</pre>
  </dd>
  <dt>Les deux scripts ci-dessus doivent être rendus exécutable :</dt>
  <dd>`# chmod +x /usr/sbin/smb*mount`</dd>
  <dt>D'autres paquets sont nécessaire pour la gestion des montages</dt>
  <dd>`# apt install cifs-utils smbclient winbind libpam-mount-bin`</dd>
</dl>

### Configuration précise de pam_mount ###

<dl>
  <dd> 
    Le fichier <code>/etc/security/pam_mount.conf.xml</code> doit contenir
	des lignes spécifiques au service KWARTZ et à ses partages réseau.
	<br/>
	Le but est de faire apparaître clairement à l'utilisateur les
	partages <strong>Personnel</strong>, <strong>Public</strong> et
	<strong>Commun</strong> sur son bureau,
	et de faire le montage réseau de façon dynamique.
	<br/>
	Les lignes à ajouter se situent en dessous du commentaire
	<code>&lt;!-- Volume definitions --&gt;</code>.
	<br/>
	L'option <code>"vers=1.0"</code> dit d'utiliser le protocole SAMBA version 1 ;
	on peut tenter d'autres versions, mais à la date d'écriture de ce
	document, la version par défaut fait que certaines applications,
	dont LibreOffice, sont très lentes à exploiter les documents des
	partages. L'utilisation du protocole de version 1 résout ce souci.
  </dt>
  <dd>
    Contenu du fichier `/etc/security/pam_mount.conf.xml` :
<pre>
&lt;?xml version="1.0" encoding="utf-8" ?&gt;
&lt;!DOCTYPE pam_mount SYSTEM "pam_mount.conf.xml.dtd"&gt;
&lt;!--
	See pam_mount.conf(5) for a description.
--&gt;

&lt;pam_mount&gt;
  
  &lt;!-- debug should come before everything else,
       since this file is still processed in a single pass
       from top-to-bottom --&gt;
  
  &lt;debug enable="0" /&gt;
  
  &lt;!-- Volume definitions --&gt;
  &lt;volume  user="*"  fstype="smbfs"   server="SERVEUR"   path="%(USER)"
	   mountpoint="/home/%(GROUP)/%(USER)/Bureau/Perso" options="vers=1.0" /&gt;
  &lt;volume  user="*"  fstype="smbfs"   server="SERVEUR"   path="public"
	   mountpoint="/home/%(GROUP)/%(USER)/Bureau/Public" options="vers=1.0" /&gt;
  &lt;volume  user="*"  fstype="smbfs"   server="SERVEUR"   path="commun"
	   mountpoint="/home/%(GROUP)/%(USER)/Bureau/Commun" options="vers=1.0" /&gt;
  
  
  &lt;!-- pam_mount parameters: General tunables --&gt;
  
  &lt;!--
      &lt;luserconf name=".pam_mount.conf.xml" /&gt;
  --&gt;
  
  &lt;!-- Note that commenting out mntoptions will give you the defaults.
       You will need to explicitly initialize it with the empty string
       to reset the defaults to nothing. --&gt;
  &lt;mntoptions allow="nosuid,nodev,loop,encryption,fsck,nonempty,allow_root,allow_other" /&gt;
  &lt;!--
      &lt;mntoptions deny="suid,dev" /&gt;
      &lt;mntoptions allow="*" /&gt;
      &lt;mntoptions deny="*" /&gt;
  --&gt;
  &lt;mntoptions require="nosuid,nodev" /&gt;
  
  &lt;!-- requires ofl from hxtools to be present --&gt;
  &lt;logout wait="0" hup="no" term="no" kill="no" /&gt;
  
  
  &lt;!-- pam_mount parameters: Volume-related --&gt;
  
  &lt;mkmountpoint enable="1" remove="true" /&gt;
  
&lt;/pam_mount&gt;
</pre>
  </dd>
</dl>

### Paquets pour l'authentification par le proxy réseau ###
<dl>
  <dt>On installe oidentd et inetutils-inetd</dt>
  <dd>`# apt install oidentd inetutils-inetd`</dd>
  <dt>
    Puis on configure le fichier `/etc/inetd.conf` en ajoutant la ligne
	suivante près de la fin :
  </dt>
  <dd>
<pre>
ident stream  tcp wait identd /usr/sbin/identd identd
</pre>
  </dd>
</dl>


## Création de répertoires pour les groupes d'utilisateurs ##

Les utilisateurs du service KWARTZ appartiennent tous à un groupe, comme
le groupe d'une classe, le groupe des professeurs, etc. L'annuaire
spécifie que le répertoire personnel d'un utilisateur est de la forme
`/home/<groupe>/<utilisateur>`, par exemple `/home/c2d01/jeanne.dupont`.

Il est utile de créer par avance les répertoires des groupes en leur donnant
les attributs qui vont bien, afin que les créations de répertoires
automatiques à l'authentification d'un utilisateur puissent être créés
ensuite.

### Par exemple, pour le groupe `profs` : ###
<dl>
  <dt>On crée le répertoire du groupe</dt>
  <dd>`# mkdir /home/profs`</dd>
  <dt>Puis on lui donne le bon identifiant de groupe</d>
  <dd>
    `# chown :profs /home/prof`
	<br/>
	Notez bien que cette commande-là ne peut bien fonctionner que si
	le groupe `profs` existe, et surtout si le service `nslcd` est correctement
	configuré : c'est ce service qui interroge l'annuaire pour en savoir
	plus sur les groupes.
  </dd>
  
</dl>

## Définition du shell des utilisateurs ##

Sous Gnulinux, chaque utilisateur doit avoir un shell, c'est à dire un
interpréteur de commande, associé à son compte. L'annuaire de KWARTZ est
ainsi fait que ce shell est `/bin/kwartz-sh` ; afin que chaque utilisateur
dispose d'un shell correct on crée un lien symbolique de `/bin/kwartz-sh`
vers `\bin\bash`.

<dl>
  <dt>Création du lien symbolique vers le shell</dt>
  <dd><code># ln -sf bash /bin/kwartz-sh</code></dd>
</dl>

# License de ce document ![Logo CC-BY-SA 4.0](img/cc-by-sa88x31.png) #

Vous êtes libre de copier ce document, de le distribuer tel quel ou
modifié ; la licence CC-BY-SA vous oblige cependant à citer le ou les
auteurs, et à transmettre le texte de cette même license avec le
document, qu'il soit ou non modifié.

L'énoncé complet de la licence **Creative Commons - By - Share Alike**
peut être téléchargé depuis 
[https://creativecommons.org/licenses/by-sa/4.0/legalcode](https://creativecommons.org/licenses/by-sa/4.0/legalcode)

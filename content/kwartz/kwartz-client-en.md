title:       The package Kwartz-client
subtitle:    Method checked in lycée Jean Bart
lang: en
slug:     kwartz-client
author:      Georges Khaznadar
date:        2019-10-20
category: Kwartz



# Configure a Debian host for a Kwartz server, or for some LDAP service #
In many French secondary schools, a LDAP service is provided by a Kwartz
server (see https://www.kwartz.com). Hence the name of the package,
<tt>kwartz-client</tt>. However this package can be used for any other
LDAP directory, provided one understands the questions used during the
configuration of the package.

## The package kwartz-client ##

When this article was published, the package <tt>kwartz-client</tt> is
in the official Debian repository, as a candidate for the next stable
Debian distribution, *bullseye*. Its description and download links can be
accessed from https://packages.debian.org/unstable/kwartz-client

Dependencies of the package <tt>kwartz-client</tt> are:
<ul>
<li>libpam-ldapd,</li>
<li>libnss-ldapd,</li>
<li>libpam-modules,</li>
<li>cifs-utils,</li>
<li>smbclient,</li>
<li>winbind,</li>
<li>libpam-mount-bin,</li>
<li>oidentd,</li>
<li>inetutils-inetd,</li>
<li>hxtools</li>
</ul>

All those dependencies can be found in the distribution Debian 10
(code name <tt>buster</tt>).

Let us suppose below that one has a Debian 10 distribution, already installed
and active on the host being configured, and that one has super-user 
permissions. The prompt `# ` starting command lines is a reminder that the
commands must be validated by the `root` user.

## Steps in kwartz-client's install ##

As the package kwartz-client is not yet part of Debian 10, but that its
dependencies are part of this distribution, one must begin by a direct
download of the package's archive; for example if the version is 1.0-4,
the archive can be downloaded from http://ftp.de.debian.org/debian/pool/main/k/kwartz-client/kwartz-client_1.0-4_all.deb

### (partial) installation of the package kwartz-client ###

Let us suppose that the package <tt>kwartz-client_1.0-4_all.deb</tt> is
already downloaded; then one must install it with the command:
`# dpkg -i kwartz-client_1.0-4_all.deb`

The archive will be uncompressed, but very probably not completely installed:
one missing dependency is enough to prevent the full installation.

### install dependencies of the package kwartz-client ###
Then the easiest way is to ask for an automatic search of missing packages
with the command  `apt`. This implies that one has access to Internet. If one
is `root` on the host, and that Internet can be reached by the proxy server
provided by Kwartz, one can define the proxy so:

`# export http_proxy=<kwartz' IP address>:3128`

When the variable `http_proxy` is properly defined, on calls the command:

`# apt update`

to update the list of available packages ; if it does not work, one must
configure the Kwartz server to give access to Internet with no filter, based
on the host's IP address. One can also satisfy Kwartz' access policy by
installing the package `oidentd`, but it becomes something like a
chicken-and-egg issue.

When one uses `apt`'s features, it can automatically add dependencies of
wanted packages. When the command  `apt update` is successful, one can
launch `apt -f install`, which checks for inconsistencies in the
installed packages, and then download, install, configure dependencies of
<tt>kwartz-client</tt>. Questions will be asked during the configuration
of dependencies ... it does not harm if one replies a mistake at that moment,
because ... *be careful* ... when the configuration of <tt>kwartz-client</tt>
begins, every important question will be asked once more, and then one must
provide the right response.

## kwartz-client's configuration dialog ##

Before beginning to configure <tt>kwartz-client</tt>, one should prepare
a short paper form, and fill it with the help of the network's admin.
Here is the form:

> **IP address of the LDAP server** : .......................................<br/>
> this address can be some numbers like 172.16.0.254, but it can also be
> a symbolic name like serveur.lycee.jb, for instance
>
> **Port of the LDAP service when it is not standard** : ....................<br/>
> The standard port number is 389; in a few cases the LDAP service should be
> accessed via some other port number, for instance 1389.
>
> **The base to begin searches from in the directory** : ....................<br/>
> Most often, it matches the ends of the symbolic IP address of the server, i.e.
> its domain name. If the IP address was serveur.lycee.jb, then the base is:<br/>
> dc=lycee,dc=jb
>
> **The path to a user known in the database** : ............................<br/>
> From year 2018 on, one can no longer access Kwartz' LDAP directory in a
> completely anonymous mode. So, one must know the name of one user.
> Hint for the network's admin: the user must be part of some non-privileged
> group, but one must ensure that this user will not be erased later during
> some maintenance. So, when there are groups of students, teachers, or other
> real persons, one should avoid using such a group where people can be erased
> some day. Here is an example, where a user of login "personne" was defined:<br/>
> cn=personne,cn=Users,dc=lycee,dc=jb
>
> **This user's password** : ................................................<br/>
> Every user in the directory has his password. For the powerless user defined
> above, one must know the password (avoid using too simple passwords).
>
> **The NETBIOS name of the Kwartz** : ....................................<br/>
> It is the name of the Kwartz server as it appears in a Windows neighborhood.
> For example:<br/>
> SERVEUR

When the paper for is filled, one must prepare the LDAP URI of the server.
<dl>
<dt>Simple syntax</dt>
<dd>The port number is standard (i.e. 389); the the URI is:<br/>
ldap://&lt;server's IP address>/</dd>
<dt>Complete syntax</dt>
<dd>The port number is not standard; then the URI is:<br/>
ldap://&lt;server's IP address>:&lt;port number>/</dd>
</dl>

*Et voilà*, you are ready to configure the package. Run the command
`sudo dpkg-reconfigure kwartz-client` whenever you want to replay 
the configuration dialog.

# kwartz-client is installed, what next? #

If <tt>kwartz-client</tt>'s configuration is correct, and that the host is
well connected to the local network enjoying Kwartz server' services, then
one should be able to *change the user*, and authenticate on the Kwartz
network: usually with a login like first_name.last_name, and the associated
password.

If there is a mistake in the configuration, the the authentication fails.
One can configure the package again, with the command
`# dpkg-reconfigure kwartz-client`

When the authentication succeeds, the authenticated user is provided a
graphic desktop, and will find on it three mount points for network
shares:
<dl>
<dt>Perso</dt>
<dd>to access personal data, including the directory **Travail**,
which Windows may sometimes present like a directory « My Documents »,
inside the network device named <tt>H:</tt>;</dd>
<dt>Commun</dt>
<dd>to access « commons », belonging to each groups where the user is
subscribed. Common data are read-write;</dd>
<dt>Public</dt>
<dd>to access « public » data, belonging to each groups where the user is
subscribed. Common data are read-only for students, read-write for members of the teacher group.</dd>
</dl>

# License of this document ![Logo CC-BY-SA 4.0](img/cc-by-sa88x31.png) #

You are free to copy this document, to share it likewise or with some
modifications; however the license CC-BY-SA makes mandatory to cite authors,
and to distribute the document with this same license, whether it is modified
or not.

The complete text of the license **Creative Commons - By - Share Alike**
can be downloaded from 
[https://creativecommons.org/licenses/by-sa/4.0/legalcode](https://creativecommons.org/licenses/by-sa/4.0/legalcode)

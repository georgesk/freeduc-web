title:  Des clients GNU/Linux au lycée Jean Bart
lang: fr
slug:     linux-kwartz-jb
author:      Georges Khaznadar
date:        2019-05-11
category: Freeduc-JBART



# Des clients Linux, rapides et robustes #

La distribution [Freeduc-JBART](jbart.html), depuis l'année 2019, est basée
sur <i>Debian-Live</i>, ce qui permet d'en faire un système GNU/Linux bien
adapté à une installation et un fonctionnement sur le disque dur d'un
ordinateur.

Les versions précédentes étaient basées sur un système <i>KNOPPIX</i>, légèrement
plus rapide quand on le lance depuis une clé USB, mais qui n'est pas bien
conforme aux standards d'une installation Linux classique quand on
l'installe sur disque dur.

## Version papier de ce document ##

Ce document et d'autres pages annexes sont téléchargeables 
[au format PDF](/img/kwartz-linux-live-book.pdf).

## Quelques avantages ##

<dl>
<dt>La taille du système</dt>
<dd>Le système complet tient sur quelques giga-octets : le minimum est
de huit Go. Si on l'installe sur un disque, le reste est disponible pour
l'utilisateur.</dd>

<dt>Un bureau graphique agréable</dt>
<dd>Le bureau graphique est basé sur <i>Cinnamon</i>, une variante du bureau
<i>Gnome</i>, actuellement maintenue par l'équipe dynamique qui est à l'origine de
<a href="https://linuxmint.com/">Linux Mint</a>.</dd>

<dt>Le système est à l'épreuve des débutants</dt>
<dd>Quand on veut apprendre l'informatique, autant laisser les débutants,
et les moins débutants, faire <i>tous les essais</i> qu'ils peuvent imaginer.
De toute façon, ils les feront, consciemment ou non.<br/>
Arrive le moment où l'ordinateur est cassé : plus rien ne fonctionne, et
on se rend compte (OUPS !) que l'utilisateur a effacé tout ce qu'il pouvait.
<br/>
Une installation raisonnée de la distribution <i>Freeduc-JBART</i> permet
de rattraper les dégâts d'une façon originale : toute modification faite
par un utilisateur, y compris un effacement, correspond à une écriture sur
une partition précise du disque, cependant que le système, qui est en
lecture seule, n'est jamais modifié. <strong>Il suffit donc d'effacer
le dossier de persistance où résident les traces de l'utilisateur</strong>,
et l'ordinateur revient à sa « configuration d'usine ».
</dd>

<dt>La logithèque accessible est grande</dt>
<dd>Freeduc-JBART est basée sur Debian/Buster, qui fournit quelques
cent mille paquets distincts, dont une part sont des logiciels
directement accessibles dans l'interface utilisateur.
<ul>
<li>L'installation de nouveaux paquets est simple</li>
<li>Chaque paquet logiciel est signé par son responsable</li>
<li>Les paquets obéissent aux <strong>principes du logiciel libre selon Debian</strong>,
qui protègent les utilisateurs et leurs données personnelles.</li>
</ul>
</dd>

</dl>

## Déploiement de machines frugales ##

<img src="/img/petitpc.png" alt="un très petit PC" style="float:right;"/>
À contre-courant de la tendance qui veut qu'à tout moment, les utilisateurs
de technologies s'équipent du matériel le plus puissant du marché adapté
à leur bourse, le lycée Jean Bart prend le parti d'utiliser des machines
moins puissantes, mais tout à fait adaptées à l'usage d'enseignement.

Voici une liste de raisons pour ce choix :

1.  Nous installons un système GNU/Linux, et uniquement des paquets de la distribution Debian. Tous ces paquets sont sous des licences libres, vérifiées et certifiées par les responsables de chaque paquet individuel. Par conséquent, il devient inutile de faire fonctionner en permanence des logiciels de vérification, tels que des anti-virus issus de tierces parties, ou de scruter en permanence les faits et gestes de l'utilisateur et en référer par Internet à une tierce partie, comme le font une majorité d'ordinateurs grand public.
2.  La place nécessaire sur le disque est minimisée, par le fait qu'une distribution comme Debian recourt à des bibliothèques partagées par tous les logiciels de la distribution. Ainsi, les logiciels utilisateurs n'ont pas besoin de venir avec un paquet obèse, rempli des bibliothèques utilisées par eux seuls.
3.  Un ordinateur grand public ordinaire est dimensionné pour consommer une puissance électrique de 50 W. Nos petits ordinateurs consomment plutôt 5 W ou moins.
4.  Au lycée Jean Bart, les élèves doivent s'habituer à ne pas déposer de fichiers personnels sur le disque dur de la machine locale ; il est préférable d'utiliser une mémoire Flash personnelle, ou d'enregistrer les fichiers dans un partage disque mis à disposition par le service Kwartz.

# L'architecture matérielle #

<ul>
<li> Un PC avec un processeur à deux cœurs 64 bits, compatible Intel/AMD</li>
<li> Une mémoire vive de 4 Go</li>
<li> Un disque dur solide (SSD) de 60 Go</li>
<li> Connectique :
  <ul>
   <li>quatre prises USB3</li>
   <li>vidéo VGA et HDMI</li>
   <li>Ethernet gigabit</li>
   <li>Entrée microphone, sortie écouteur</li>
   <li>(sans fil) WIFI et Bluetooth</li>
 </ul>
</ul>

# Architecture logicielle #

* deux partitions formant ce qu'on appelle un disque ISO-Hybride, c'est à dire qui se présente comme un DVD-ROM (en lecture seule), et de quoi démarrer sur un système UEFI ;
* une partition de persistance montée en *overlay* sur tout le système de fichiers, qui contient les adaptations au service Kwartz ;
* une partition de persistance montée en *overlay*, sur une partie du système de fichiers, pour donner toute liberté à l'utilisateur, y compris faire des mises à jour et installer des paquets logiciels.

## Principe du montage en *overlay* ##

<img src="/img/disque.svg.png" alt="principe du montage"/>

Sur la figure ci-dessus, on voit le disque partagé entre le système
ISO-Hybride, et deux autres partitions, de type ext4.

Le système ISO-Hybride est fabriqué à l'aide de *live-build*, à partir
des paquets de Debian/Buster. Ça fournit un système de démarrage (boot
UEFI et boot Legacy), et un arbre complet de fichiers en lecture seule.

La partition 3, en bleu clair, fournit un arbre de fichiers montés en
*overlay*, masquant le système précédent. Le principe de l'overlay est
que si un fichier existe dans le montage issu de la partition3, il
cachera tout fichier de même nom situé dans la partition 1 (en orange
clair). Par exemple, le fichier qui contient Y cache le fichier de même
nom qui contient C. Si on accède à ce fichier, on lira **Y et non pas C**.

Dans la partition 3, il y a les modifications qui permettent à la
distribution de s'adapter au service Kwartz du lycée Jean Bart.

Dans la partition 4, en bleu plus foncé, on trouve plusieurs arbres de fichiers
qui sont montés en *overlay*, cachant les partitions 3 et 1. L'utilisateur
écrit dans les overlays de la partition 4 seulement. Le principe de cacher des
données situées sur d'autres partitions fonctionne de même. Ainsi, pour un
fichier de même nom existant sur chacune des partitions, seul celui de la
partition 4 sera accessible. Par exemple, on lira **Z, mais ni X, ni B**.

## Et si on efface ? ##

Quand un utilisateur efface un fichier, cela inscrit un marque dans l'overlay
qui dit « le fichier est effacé ». Par exemple, supposons que l'utilisateur
efface le fichier qui contient A, et qui n'est que dans la première partition ;
alors, la partition 4 contiendra une marque qui dit 
« ce fichier-là est effacé ».

Si l'utilisateur veut lire ce fichier, il recevra le message d'erreur signifiant
« le fichier n'existe pas ».

# La protection contre les fausses manœuvres des débutants #

En utilisation courante, les débutants, et les moins débutants, vont
opérer dans toutes les zones de l'arbre de fichiers qu'ils peuvent
toucher, et qui ont de l'importance pour lancer des applications,
programmer, etc.

Si une fausse manœuvre rend l'ordinateur inutilisable, c'est que la
structure des fichiers nécessaires pour le système dans son ensemble,
ou pour l'interface graphique, est altérée. Par exemple, un fichier
ou un répertoire très important a peut-être été effacé. Comme toutes
les traces de l'utilisateur se situent dans la partition 4, ...

<strong>... il suffit d'effacer toutes les traces des utilisateurs
de la partition 4 pour ravoir un système « sorti d'usine »</strong> ;
bien sûr, tous les utilisateurs ayant enregistré des données en local
les perdent, ils perdent aussi leurs personnalisations, mais le système
est utilisable à nouveau.

# Comment créer un ordinateur comme celui-là #

## Choix de l'ordinateur ##

**Live-build** crée un système GNU/Linux très riche en pilotes de matériels,
qui dispose de programmes d'auto-configuration puissants. Une très grande
majorité des ordinateurs personnels qu'on trouve en ce moment sont supportés.

Ce qui serait recommandable, pour des personnes soucieuses de leur
empreinte environnementale, c'est :
-  soit utiliser un ordinateur ancien, voire reconditionné, pour augmenter su durée de vie avant recyclage ultime : ce n'est pas très économique pour la puissance électrique consommée durant l'allumage de l'ordinateur, mais éviter à une machine de devenir un déchet ultime pendant quelques années, c'est bon à prendre ;
- soit utiliser un ordinateur neuf, en privilégiant une configuration robuste (peu de pannes prévisibles), et une faible consommation électrique. Contrairement à ce qu'on constate pour les systèmes Windows, tout permet de penser que dans dix ou quinze ans, il sera possible que l'ordinateur fonctionne à la même vitesse qu'au moment de l'achat ; sur une telle durée, le choix d'une basse puissance électrique peut compter.

## Test de l'ordinateur ##

Le plus simple est de démarrer l'ordinateur par une clé USB portant le
système [Freeduc-JBART](jbart.html) ; si on voit que l'ordinateur fonctionne
bien, il fonctionnera bien aussi après configuration de son disque dur.

## Préparation du disque dur ##

### Copier le système ISO-Hybride vers le disque dur ###

Pour cela, on peut démarrer l'ordinateur à l'aide d'une clé USB 
[Freeduc-JBART](jbart.html), puis taper la commande suivante :
<pre>
&#35; dd if=/dev/sdb of=/dev/sda status=progress bs=4M oflag=dsync
</pre>
Attention ! adaptez la commande ci-dessus. Dans ce cas particulier,
on suppose que la clé USB est accessible comme l'appareil `/dev/sdb` et
le disque dur comme `/dev/sda` ; il pourrait en être autrement.

### Créer une partition de persistance pour l'adaptation au kwartz ##

Quand la copie est terminée, lancer la commande
`sudo cfdisk /dev/sda`

Quand on a l'interface graphique qui représente le contenu du disque dur
(supposément `/dev/sda`), on opère les modifications suivantes :
- suppression de la troisième partition
- création d'un troisième partition, de type Linux, et de taille environ 2 giga-octets
- on écrit la nouvelle partition et on sort

Il faut ensuite formater la partition 3, avec la commande
`sudo mkfs.ext4 -L persistence /dev/sda3`

### Préciser l'usage de la partition de persistance ###
La partition de persistance est créée, il importe alors d'y inscrire un fichier
qui en précise l'usage.

- créer un point de montage : `sudo mkdir /tmp/pp`
- monter la nouvelle partition : `sudo mount /dev/sda3 /tmp/pp` (on suppose que la partition à monter est `/dev/sda3`, à adapter selon le cas)
- inscrire un nouveau fichier vide : `sudo touch /tmp/pp/persistence.conf`
- éditer le fichier `/tmp/pp/persistence.conf`, il doit contenir les lignes suivantes :
<pre>
/bin union
/boot union
/dev union
/etc union
/lib union
/media union
/mnt union
/opt union
/proc union
/root union
/run union
/srv union
/sys union
/tmp union
/usr union
/var union
</pre>
- démonter la partition : `sudo umount /tmp/pp`

### Redémarrer l'ordinateur, depuis son disque dur ###

À ce stade, le disque dur de l'ordinateur est prêt à fonctionner.

Il est normal que le premier démarrage soit un peu long, car un programme
va analyser la configuration matérielle et inscrire de nombreux réglages
dans la zone de persistance.

À ce démarrage, comme aux démarrages suivants, l'ordinateur est opérationnel,
avec une utilisateur nommé `user`, pour lequel une session graphique s'ouvre,
avec le bureau Cinnamon, à chaque démarrage. L'utilisateur `user` fait
partie du groupe sudo, c'est à dire qu'il est habilité à préfixer des
commandes par `sudo`, ce qui lui donne des droits de super-utilisateur,
quand il le fait.

## Intégration dans un réseau Kwartz ##

Le système [Freeduc-JBART](jbart.html) n'a pas les bons outils ni la
bonne configuration pour une intégration poussée à un réseau Kwartz.

### Inscription de la machine dans le réseau ###

On peut lire l'adresse MAC de la carte réseau en lançant la commande
`/sbin/ifconfig` ; l'interface Ethernet est celle dont le nom commence par
la lettre « e ». Connaissant cette adresse, on peut inscrire la machine
dans le réseau Kwartz (grâce à Kwartz~Control, l'application web
accessible à `https://<IP du serveur Kwartz>:9999/`). L'inscription
peut se faire à partir d'un autre ordinateur du réseau.

Quand c'est fait, le nouvel ordinateur est reconnu par le réseau Kwartz.
Il peut recevoir une adresse IP dynamique de la part du service DHCP.

### Mise à jour de la liste des paquets ###

Quand l'ordinateur possède la connectivité convenable pour accéder à Internet,
il faut lancer les commandes suivantes :

-  `sudo su` pour devenir super-utilisateur, pour la suite ;
-  `export http_proxy=http://<IP du serveur Kwartz>:3128` pour pouvoir accéder à Internet à travers le service de proxy, pour les commandes en ligne suivantes ;
-  `apt update` pour mettre à jour les catalogues de paquets logiciels ; il y en a quelques cent mille...

### Mise en place de l'intégration au réseau Kwartz ###

Ce sujet est traité dans une autre page :
<a href="/kwartz4all.html" title="Cliquer pour ouvrir dans un nouvel onglet" target="_new">
 Adapter une station Debian à un serveur Kwartz
</a>

Il faudra se souvenir que pour toute manipulation en tant que super-utilisateur,
il faut refaire les commandes en ligne suivantes :

-  `sudo su`
-  `export http_proxy=http://<IP du serveur Kwartz>:3128`

La mémoire de l'historique des commandes permet de retrouver facilement
les lignes déjà tapées dans le passé. Par exemple, si on tape :
`Ctrl-R expo`, il y a beaucoup de chances que cela rappelle la ligne de
commande permettant le réglage du proxy.

### Test de l'intégration au réseau Kwartz ###

Quand les réglages décrits dans la page 
<a href="/kwartz4all.html" title="Cliquer pour ouvrir dans un nouvel onglet" target="_new">
 Adapter une station Debian à un serveur Kwartz
</a> sont terminés, on est toujours avec un ordinateur qui ouvre par défaut
une fenêtre graphique avec un environnement de bureau pour `user`.

À ce stade, c'est une bonne idée de définir un mot de passe pour `user`,
en faisant ainsi :
-  `sudo passwd user`
- entrer deux fois le mot de passe pour `user`, et ne pas l'oublier tout de suite.

À l'aide des menus graphiques, on peut fermer la session de `user` ; on arrive
alors à la fenêtre graphique de connexion. Là deux contrôles sont utiles :

1. qu'on peut toujours se connecter sous `user` avec le mot de passe qu'on n'a pas oublié ;
2. qu'on peut se connecter sous un autre login, du type `prenom.nom`, connu dans l'annuaire du Kwartz ; si tout se passe bien, on doit avoir à disposition un bureau graphique, sur lequel se trouvent des icônes pour accéder aux partages de disques hébergés par le service Kwartz.

On se reconnecte alors comme `user`, et on reconfigure l'ordinateur pour
que celui-ci n'ouvre plus au démarrage une session graphique pour `user` :
la fenêtre graphique doit présenter le dialogue d'authentification.

Quand c'est fait, on redémarre l'ordinateur, on vérifie que celui-ci est
normalement intégré au réseau Kwartz : chaque utilisateur connu du réseau peut
se connecter, il dispose des accès à ses zones de disques partagées par Kwartz.

### Sauvegarde de la configuration ###

C'est le bon moment pour faire une sauvegarde du système, par exemple avec
[Clonezilla](https://clonezilla.org/) ; à ce stade, la sauvegarde tient
facilement sur une clé USB de 16 Go.

### Création de la zone de persistance pour les utilisateurs ###

Un élève n'est pas censé pouvoir casser facilement l'intégration dans
le réseau Kwartz, pas plus qu'il n'est censé pouvoir casse le système GNU/Linux.

C'est pourquoi une partition séparée doit être créée, qui supportera toutes
les traces des élèves.

Pour ne pas garder de fichier inutiles, on détruit les enregistrements de
catalogues de paquets logiciels : `sudo rm -rf /var/cache/apt/*`

On redémarre l'ordinateur par un clé USB [Freeduc-JBART](jbart.html),
on relance la commande `sudo cfdisk /dev/sda` ; les manœuvres à faire alors
sont :

1. créer une nouvelle partition,  de type Linux ; cette partition occupe toute la place restante
2. enregistrer la nouvelle table de partition et sortie du programme cfdisk.

On formate la dernière partition, avec le label "persistence" aussi :
`sudo mkfs.ext4 -L persistence /dev/sda4`

Après création et montage de la nouvelle partition, on fait en sorte d'y créer un fichier `persistence.conf`, qui contient une seule ligne :
<pre>
/ union
</pre>
Cette ligne, en quelque sorte, donne l'accès à toute modification, en tout point du disque, pour les utilisateurs. Cependant, ceci se fait sans altération des partitions 1 et 3 qui contiennent les « réglages d'usine ».

# Sauvegarder le disque dur, maintenant complet #

À ce stade, le disque dur est complet, la zone de persistence pour
les utilisateurs protège tous les « réglages d'usine ». C'est le bon moment
pour faire une sauvegarde du disque dur, qui tient sur moins de 16 giga-octets,
si on utilise un bon logiciel comme [Clonezilla](https://clonezilla.org/), qui
ne copiera que les secteurs du disque utilisés.

# Déployer la solution #

La sauvegarde, si elle est déployée sur d'autres machines similaires ou
à peu près similaires, sera tout de suite fonctionnelle.

# Réparer une machine « cassée » #

Pour effacer la zone de persistance des utilisateurs, on peut
démarrer à partir d'un support externe, par exemple une clé USB
[Freeduc-JBART](jbart.html), et taper :
<pre>
sudo su
mkdir /tmp/pp
mount /dev/sda5 /tmp/pp
rm -rf /tmp/pp/run/live/persistence/rw
umount /tmp/pp
</pre>
Puis, on redémarre depuis le disque dur.

# Bugs connus, piste d'améliorations, contact #

L'auteur du document, Georges Khaznadar, peut être contacté à l'adresse
`georgesk@debian.org`.

## Bug : engorgement du disque dur ##

Dans un établissement scolaire, un ordinateur est banal, il peut être utilisé
par des dizaines d'élèves qui s'y connecteront.

Pour chaque utilisateur, un arbre de fichiers sur le disque local est construit,
et certaines applications peuvent y déposer beaucoup de données. Par exemple,
Firefox peut y déposer les données de son cache. Si ce cache contient des
vidéos, cela peut rapidement représenter des giga-octets.

Si le disque dur est trop plein, la machine peut refuser

## Pistes pour des améliorations ##

-  On peut rêver d'un paquet Debian qui enchaînerait les étapes décrites dans le document <a href="/kwartz4all.html" title="Cliquer pour ouvrir dans un nouvel onglet" target="_new"> Adapter une station Debian à un serveur Kwartz</a>, après un dialogue de configuration bref ;
- La procédure de réparation, retour aux « réglages d'usine » pourrait être une des options de démarrage du disque ;
- On pourrait faire en sorte de limiter la taille des données conservées pour chaque utilisateur à quelques centaines de méga-octets, en effaçant au besoin des données en cache à la fin d'une session graphique ;
- ...

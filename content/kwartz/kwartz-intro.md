title:    Le service Kwartz
lang: fr
slug:     kwartzIntro
author:      Georges Khaznadar
date:        2019-05-11
category: Kwartz



# Les serveurs Kwartz #

KWARTZ Serveur intègre dans une solution logicielle homogène de
nombreux composants logiciels: (Samba / Apache / Php / MySql / Exim /
OCS / Webdav / Owncloud / Squid / Roundcube).

KWARTZ Serveur est une solution commerciale développée par la société
[IRIS Technologie](https://www.kwartz.com/fr/nos-produits/kwartz-server),
qui a été largement déployée dans les établissements scolaires du
Nord-Pas de Calais.

L'utilisation de la distribution [Freeduc-JBART](/jbart.html) est facilitée
au sein des établissements d'enseignement, si on prend en compte :
-  l'authentification des utilisateurs sur l'annuaire LDAP du serveur Kwartz ;
-  le filtrage des contenus Web organisé par le service Squid ;
-  les dossiers partagés du service Samba.

Pour cette raison, un article explique 
[comment on peut configurer](/kwartz4all.html) une
station GNU/Linux pour son intégration dans un réseau contrôlé par
Kwartz.

title:  Un coffret pour 20 clés Freeduc
lang: fr
slug:     coffret-20-freeduc
author:      Georges Khaznadar
date:        2019-10-28
category: Freeduc-JBART



# Un coffret pour 20 clés Freeduc #

Les élèves de SNT (Science du Numérique & Technologie) du lycée Jean Bart, comme
tous les élèves de ce type d'enseignement, n'ont pas choisi de suivre ce cours
en particulier : il s'agit d'un tronc commun d'enseignement. Le programme de
SNT laisse le libre choix du système d'exploitation ... excellente occasion
pour la découverte des logiciels libres !

<img src="/img/boite/boite-ouverte-3.jpg" alt="boite ouverte, clés numérotées" style="width: 250px; float: left; padding-right: 1em;"/>
Comme il s'agit d'un enseignement en tronc commun, nous avons préféré que les
familles n'aient pas à engager de dépenses particulières ; c'est sur les fonds
d'enseignement que le lycée équipe chaque enseignant qui intervient en SNT
d'une vingtaine de clés USB Freeduc. Comme en général les élèves les utilisent
en groupes de dix-sept élèves au plus, le nombre de clés est convenable.

Les clés USB sont identifiées par un numéro, et elles sont prêtées chaque fois
aux même élèves ; par exemple, la clé numéro 5 sera prêtée à l'élève X... de la
classe de seconde 4 groupe A, à l'élève Y... de la classe de seconde 4 groupe B,
 à l'élève Z... de la seconde 5 groupe A, etc. Ça permet
de faire un suivi minimal de la qualité des clés. Les élèves ne s'approprient
pas les clés USB, mais ils savent qu'à n'importe quelle occasion, ils peuvent
[cloner la clé](live-clone.html) qui leur est prêtée, sur une clé USB
personnelle de 16 giga-octets (ou plus).

# Réalisation des coffrets #

[<img src="/img/boite/presentoir-festibox.png" alt="extrait du fichier de découpe" style="width: 400px; float: left;"/>](/img/boite/presentoir-festibox.svg)
Les coffrets sont réalisés au
[Fablab de Dunkerque ](https://www.spark-dunkerque.fr), elles sont en
médium (MDF d'épaisseur 3 mm), découpé à l'aide d'un table de découpe au laser,
puis assemblées avec de la colle à bois. Notez bien que même sans collage, la
boîte est fonctionnelle, juste un peu moins résistante aux chocs ; en effet
l'ajustement des assemblages est juste assez serré pour nécessiter un effort
modéré (un peu plus que pour des pièces de LEGO), pour emboîter/déboîter
les pièces.


Voici [un lien vers le fichier de découpe](/img/boite/presentoir-festibox.svg) au format SVG. Les épaisseurs de
traits et les couleurs choisies correspondent aux conventions d'une découpeuse
laser de marque Trotec : trait bleu, en épaisseur de 0,1 mm c'est pour couper,
trait rouge, en épaisseur 0,1 mm c'est pour graver.

Les logos qui apparaissent sur le couvercle sont réutilisables tels quels mais
le mieux est de les personnaliser pour chaque petite série.

La boîte s'ouvre et se ferme grâce à une série de découpe nommée « *flex* », qui
permet de jouer sur la plasticité du médium pour le plier et le déplier.
Grâce au *flex*, une force de rappel douce permet au boîtier de ne pas s'ouvrir
spontanément après qu'il ait été fermé, car il faut tirer un peu sur le flex
pour que le couvercle passe en position fermée. Attention cependant, quand
les vingt clés sont dans la boîte, on ne peut pas soulever par le couvercle
sans que celui-ci s'ouvre à cause du poids de celles-ci. Un bracelet élastique
permettra de sécuriser la fermeture du boîtier.

# Distribution des clés #

Les élèves viennent chercher les clés dans le coffret en début de séance, 
puis les remettent en place en fin de séance. Le coffret contient une petite
béquille qui permet de maintenir le couvercle levé. Il y a la place pour
quelques feuilles de papier, par exemple les listes d'élèves avec leur
numéro attitré. On peut ajouter une feuille pour les déclarations de problèmes
matériels.

## Quelques problèmes, et leur solution ##

<dl>
<dt>La clé ne boote pas</dt>
<dd>On peut vérifier si le BIOS de l'ordinateur est réglé pour donner la
priorité à une clé USB pour le démarrage, quand celle-ci est présente.
Si cette vérification est faite, essayer avec une autre clé Freeduc. Si l'autre
clé Freeduc fonctionne, il est peut-être indiqué de régénérer la clé fautive
à l'aide de l'application  <a href="live-clone.html">live-clone</a>, à partir
de la clé qui fonctionne bien.
</dd>
<dt>La clé boote, mais on n'arrive pas au bureau graphique</dt>
<dd>Tenter à nouveau un démarrage par la clé ; quand l'écran d'attente (celui
avec le logo tracé avec des craies de couleur) s'affiche, appuyer sur une
touche fléchée : l'écran d'attente est remplacé par le défilement de divers
messages de service, qui sont émis par le noyau Linux durant le démarrage.
Si on voit que ces messages sont remplacés par un écran noir après un moment,
cela signifie que l'ordinateur est passé dans un mode graphique différent pour
afficher le bureau graphique, mais que le bureau graphique n'est lancé dans
les temps. <br/>
Alors, on peut supposer que la clé USB a été altérée dans la partition de
persistance, par une fausse manœuvre dans le passé récent, par exemple,
la clé a été débranchée alors qu'un fichier de configuration important était
en cours d'écriture.<br/>
L'application  <a href="live-clone.html">live-clone</a>, lancée à partir d'une clé
Freeduc valide, permet de remettre à zéro la partition de persistance, ce 
qui rénove la clé fautive.
</dd>
<dt>La clé boote, le bureau est là, mais on ne peut pas s'authentifier</dt>
<dd>
Si on ne peut pas s'authentifier, il est possible de revenir à la session
<i>banale</i>, sous l'utilisateur <tt>user</tt>, à l'aide d'un appui sur les touches
Ctrl + Alt + F7 (appuyer d'avance sur Ctrl + Alt qui restent appuyées, appuyer
une seule fois sur la touche F7). Là, sous utilisateur <tt>user</tt>, on peut
lancer l'application <a href="kwartz-client.html#tableDesMatieres10">kwartz-cloud</a>. S'il est
alors possible de se connecter au Cloud avec succès, c'est que le système
d'authentification est fonctionnel (on peut essayer avec un nom
d'utilisateur dont le mot de passe est connu avec certitude). Un système
d'authentification fonctionnel, ça suppose deux choses : le paquet
<a href="kwartz-client.html#tableDesMatieres7">kwartz-client</a> est bien configuré, et la
communication dans le réseau est correcte.<br/>
Si ça ne va pas et que le réseau semble correctement fonctionner cependant,
ça vaut la peine de reconfigurer le paquet 
<a href="kwartz-client.html#tableDesMatieres7">kwartz-client</a>.
</dd>
<dt>L'écran semble cohérent, mais plus rien ne fonctionne</dt>
<dd>
Dans la quasi-totalité des cas où c'est arrivé, il s'agit d'un élève qui
a malencontreusement débranché la clé vive. Il suffit de la débrancher très
brièvement pour que le système soit définitivement gelé : c'est un peu comme
si quelqu'un débranchait le disque dur de l'ordinateur pendant le 
fonctionnement. <br/>
Seule solution : on reboote. Ça vaut la peine de rappeler quelques précautions :
faire très attention de <i>ne pas bousculer</i> la clé Freeduc pendant la session
de travail. En particulier il faut être très attentif à ça aux moment où on 
brancherait ou débrancherait quelque chose à une prise USB voisine : la clé
Freeduc ne doit pas être touchée.
</dd>
</dl>

# Galerie de photos #

<style>
div.gallery {
	height: 500px;
	overflow: auto;
	background: #ffffdd;
	border: 1px solid black;
	border-radius: 0.5em;
	padding: 0.5em;
	margin: 0.5em;
}
</style>

<div class="gallery" style="float: left; width: 600px">
<img src="/img/boite/boite-ouverte-3.jpg" alt="boite ouverte, clés numérotées" title="boite ouverte, clés numérotées" style="width: 600px; float: left;"/>
Le coffret ouvert, avec sa béquille déployée pour tenir le couvercle
</div>

<div class="gallery" style="float: left; width: 600px">
<img src="/img/boite/boite-ouverte-1.jpg" alt="boite ouverte, clés côté logo" title="boite ouverte, clés côté logo" style="width: 600px; float: left;"/>
Le coffret ouvert, la béquille est encore posée dans son rangement
</div>

<div class="gallery" style="float: left; width: 600px">
<img src="/img/boite/boite-fermee-2.jpg" alt="boite fermée" title="boite fermée" style="width: 600px; float: left;"/>
Le coffret fermé. Le logo est un jeune GNU bondissant, et un marquage au nom du lycée. On remarque la charnière faite en <i>flex</i>.
</div>

<div class="gallery" style="float: left; width: 600px">
<img src="/img/boite/cle1.jpg" alt="Clé Freeduc, côté logo" title="Clé Freeduc, côté logo" style="width: 300px; float: left; padding-right: 1em;"/>
Le lycée Jean Bart a fait produire des clés USB marquées d'un logo sur 
une face. Grâce à l'application <a href="live-clone.html">live-clone</a>, il faut
cinq minutes pour produire 4 clés à partir d'un modèle. Ensuite, on distribue
les quatre nouvelles clés chargées du contenu Freeduc, et on en génère seize
autres dans les cinq minutes suivantes. Les élèves apprennent deux choses :
une toute petite clé USB, si sa qualité est suffisante (USB3, et vitesse 
d'écriture au moins 5 méga-octes par seconde), ça permet d'avoir un système
au moins aussi réactif que le Windows présent sur la machine d'une part, et
d'autre part il existe des logiciels pour lesquels le droit de copie est acquis.
Alors, pourquoi se priver ? On peut aussi leur apprendre que les licences des
logiciels distribués les autorisent à revendre des clones de Freeduc.
</div>

<div class="gallery" style="float: left; width: 600px">
<img src="/img/boite/cle2.jpg" alt="Clé Freeduc, zone à marquer" title="Clé Freeduc, zone à marquer" style="width: 300px; float: left; padding-right: 1em;"/>
Sur la face opposée au logo, les clés commandées par le lycée Jean Bart ont
un rectangle sérigraphié en peinture blanche. À condition de nettoyer la
peinture avec un peu de produit à vitres ou d'alcool à brûler pour éliminer
les traces de graisse, on obtient un bon marquage permanent à l'aide de
feutres de marquage ordinaires. On peut aussi marquer la clé par grattage ;
rien n'empêcherait non plus de positionner les clés dans la boîte de façon
régulière, et de les marquer par brûlage de la peinture à l'aide d'une
découpeuse laser à petite puissance, dans le fablab voisin. On peut aussi
tenter un marquage direct de l'inox ; attention cependant, les fabricants de
découpeuse laser déconseillent de tenter de graver un métal <i>poli</i> ; il faut
bien vérifier au préalable que les reflets du laser sur le métal ne sont pas
spéculaires (c'est à dire qu'il faut que la puissance du laser soit dispersée
et non pas concentrée après reflet sur le métal).
</div>

<div style="clear: both"></div>


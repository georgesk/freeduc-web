title:  Live-Clone
lang: fr
slug:     live-clone
author:      Georges Khaznadar
date:        2019-10-28
category: Freeduc-JBART

# Live-Clone : la liberté de copier ! #

Nos élèves entendent depuis leur plus jeune âge que copier ce n'est pas bien,
en particulier en matière de logiciels ; dans le même temps ils voient bien que
techniquement, plusieurs méthodes de copie sont à leur portée, et quelque part
apprennent à se comporter d'une façon tordue, « illégale », voire « pirate ».

Des mots tels que la *piraterie* informatique ont été forgés par les même
compagnies qui ont longtemps combattu les logiciels libres, qui souhaitent
organiser la rareté des ressources là où techniquement la copie est la solution
simple.

<img src="/img/boite/live-clone-button.png" alt="le bouton pour cloner" style="float: left; padding-right: 1 em;"/>
Avec Freeduc, renouons avec la pratique ancestrale de partage des connaissances
et des biens communs : *toutes* les licences des contenus qu'on trouve dans
le cœur de Freeduc autorisent la copie. Donc, on s'en prive pas !

# La clé Freeduc-Jbart est auto-clonable #

Dans le menu, sous la rubrique Administration, se trouve l'application
`live-clone`, avec pour logo le « jeune GNU bondissant » 
<img src="/img/boite/live-clone-button.png" alt="le bouton pour cloner" style="width: 32px; margin-bottom: -13px;"/>

Il faut le pouvoir d'administration pour lancer l'application de clonage.
C'est le cas de l'utilisateur *banal*, autrement dit `user`, qui peut facilement
gagner ce droit. Ainsi, quand on lance l'application, il est facile de cloner
la clé Freeduc elle-même.

Notez bien : le clonage reproduit la zone système de la clé, mais crée une zone
de persistance vierge. Ainsi il n'y a pas de fuite de données personnelles lors
du clonage.

## Copie d'écran de l'application Live-clone ##
Sous la ligne des boutons d'action, on voit une clé USB détectée.
Pour démarrer un clonage, il suffit de cliquer sur l'icône du GNU ou sur le 
titre "CLONER".
<div style="text-align: center;">
<img src="/img/boite/live-clone-screen.png" alt="copie d'écran partielle de live-clone" title="copie d'écran partielle de live-clone" />
</div>

## Autres actions possibles de Live-clone ##
L'icône des engrenages permet de lancer un utilitaire de restauration d'une
clé Freeduc : si la zone de persistance est déstructurée au point que la clé
soit inutilisable (par exemple, un utilisateur, qui en a le droit, aurait
déclenché l'effacement de toute la clé !), l'utilitaire permet de reformater
la zone de persistance. Ainsi, de façon « magique », la clé Freeduc peut
renaître toute neuve : l'effacement de la zone de persistance ôte les fichiers
litigieux et permet de retrouver les fichiers soi-disant effacés.

Cet utilitaire permet au passage de sauvegarder certaines parties de la zone
de persistance, si on le veut, avant le grand nettoyage.

<img src="/img/boite/fond-mediavif.jpg" alt="GNU et Linux se répandent dans la galaxie" title="GNU et Linux se répandent dans la galaxie" style="max-width: 40%; padding: 0.5em;float: right;"/>
# Cloner : quatre, puis vingt, puis cent, puis ... #
Pour distribuer des clés USB Freeduc, rien de plus simple. 
## Première clé : compter moins de deux heures ##
Il faut déjà construire la première clé, comme expliqué à 
https://usb.freeduc.org/freeduc-usb/freeduc-jbart/index.fr.html
Ça peut être un peu long, étant donné qu'il faut télécharger 3 giga-octets :
typiquement, un peu moins d'une heure au bout d'une connexion ADSL moyenne
en France. Puis, selon qu'on utilise de bons outils ou d'excellents outils,
il faut le temps de transférer 16 giga-octets vers une clé USB. Si cette
clé est de bonne qualité (USB3, écriture plus rapide que 10 méga-octets par
seconde), ça peut durer cinq minutes, ou le tiers de ce temps avec les meilleurs
outils comme décrit dans le [mode d'emploi](https://usb.freeduc.org/freeduc-usb/freeduc-jbart/index.fr.html).

## Faire quatre clés de plus : cinq minutes ##
C'est là qu'intervient l'application `Live-clone` : elle se trouve dans les
menus, sous la rubrique Administration. Voyez son logo, ci-contre.

On branche quatre clés USB supplémentaires à l'ordinateur, directement ou par
l'intermédiaire d'un HUB. Puis on lance `Live-clone`. Il suffit de cliquer
sur le mot CLONE ou sur le jeune GNU bondissant du logo, confirmer qu'on veut
bien cloner quatre clés, et c'est parti. Quatre clés de bonne qualité, clonées
simultanément, ça dure cinq minutes.

## Monter à vingt clés : cinq minutes ##
Ensuite, si on distribue les quatre clés nouvellement initiées à quatre
élèves, et si chacun a un ordinateur à disposition, faites le calcul :
Vous aurez seize clés utilisables, cinq minutes plus tard. 4 + 16 = 20.

## Monter à 100 clés ... ###
On comprend bien que cela peut croître de façon exponentielle. Par exemple
si la salle dispose de 20 ordinateurs, on peut faire les 80 clés Freeduc
suivantes en cinq minutes, dont *grosso modo*, on est passé de 1 clé à 
100 clés en un quart d'heure, plus le temps des explications, plus le temps
des erreurs, etc.

Évidemment, cette progression ne reste exponentielle que tant qu'on dispose
d'un nombre suffisant d'ordinateurs, d'élèves informés, et de clés USB. La
limite, en pratique, est atteinte rapidement. 

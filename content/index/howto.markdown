title: How to make Freeduc-USB
lang:en
slug: howto
category: INDEX
date: 2018-07-19
modified: 2018-07-19
author: georgesk

# How to make Freeduc-USB #

The live USB thumbdrives Freeduc-USB on a derivative of the distribution
GNU/Linux Debian, named <a href="http://knoppix.net">KNOPPIX</a>.

## Méthod, in six stages ##

<ol>
<li>Get the last version of Knoppix, via `torrent`, from servers of
University of Kaiserslautern:
<a href="http://torrent.unix-ag.uni-kl.de/">http://torrent.unix-ag.uni-kl.de</a></li>
<li>With the kernel module `cloop`, the compressed contents of KNOPPIX
are copied to a non-compressed directory, named `live`. Data of this
distribution which can be acccessed without the module `cloop` are copied to
a directory named `compressed`.</li>
<li>The file tree under the directory `live` is characteristic of a
working GNU/Linux distribution. With the command `chroot`, one enters the
context of that distribution, and one applies some changes:
<ul>
<li>define repositories for Debian packages;</li>
<li>remove packages which are not kept in the final product;</li>
<li>install packages which will make the future Freeduc-USB version;</li>
<li>various updates; write some scipts to launch at startup (for instance, with 
Freeduc-Wims, launch the web server, then open the Wims service in one window
of Firefox);</li>
</ul>
</li>
<li>compress the file tree under `live`, into one or more files of a
subdirectory of `compressed`, to replace similar files coming from the
original KNOPPIX distribution;</li>
<li>adjust some *boot* parameters under
`compressed/boot/syslinux`, like the welcome screen visible during the *boot*, 
options available to end-users, keyboard layout during the *boot*, etc.
</li>
<li>create a USB thumbdrive Freeduc-USB, with two partitions, data filled into
the first partition, and ceate the *boot* sector.
</ol>

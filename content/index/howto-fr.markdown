title: Comment se fait une clé vive Freeduc-USB
lang:fr
slug: howto
category: INDEX
date: 2018-07-19
modified: 2018-07-19
author: georgesk

# Comment se fait une clé vive Freeduc-USB #

Les clés vives Freeduc-USB sont basées sur le dérivé de la distribution
GNU/Linux Debian, nommée <a href="http://knoppix.net">KNOPPIX</a>.

## Méthode, en six temps ##

<ol>
<li>Récupération de la dernière version de Knoppix, généralement à l'aide
du logiciel bittorrent, à partir de serveurs de l'Université de Kaiserslautern :
<a href="http://torrent.unix-ag.uni-kl.de/">http://torrent.unix-ag.uni-kl.de</a></li>
<li>À l'aide du module noyau `cloop`, le contenu comprimé de KNOPPIX est
recopié dans un répertoire non comprimé, nommé `live`. Les données de cette
distribution accessibles sans le module `cloop` sont recopiées dans un
répertoire nommé `compressed`.</li>
<li>L'arbre des fichiers sous le répertoire `live` est celui d'une distribution
GNU/Linux fonctionnelle. À l'aide de la commande `chroot`, on se met dans le
contexte de cette distribution, et on y apporte des modifications :
<ul>
<li>définition des dépôts de paquets logiciels de référence ;</li>
<li>retrait des paquets qui ne seront pas utiles dans le produit fini ;</li>
<li>installation des paquets qui formeront la version à venir de Freeduc-USB ;</li>
<li>mises à jour diverses ; écriture de quelques scripts à exécuter au 
démarrage (par exemple, pour Freeduc-Wims, lancement du service web et
ouverture du service Wims dans une fenêtre de Firefox) ;</li>
</ul>
</li>
<li>compression de l'arborescence sous `live`, dans un ou plusieurs fichiers
d'un sous-dossier de `compressed`, en remplacement des fichiers précédents
issus de la distribution KNOPPIX ;</li>
<li>ajustement de quelques paramètres de démarrage sous 
`compressed/boot/syslinux`, tels que l'écran d'accueil lors du *boot*, 
les options préparées pour les utilisateurs, la disposition du clavier lors
du *boot*, etc.
</li>
<li>création d'une clé Freeduc-USB, avec mise en place des deux partitions,
écriture des données et mise en place du secteur de *boot*.
</ol>

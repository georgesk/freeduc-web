title: À propos des clés USB vives
lang:fr
slug: index
category: INDEX
date: 2017-12-29
modified: 2019-03-30


# Freeduc-USB : des médias vifs ! #

<img alt="Logo d'OFSET" src="{attach}/img/logo-with-ofset-trame.png" 
     style="padding: 20px; width: 40%; float: left;"/>

 Les anglais appellent live media des petits objets faciles à manier, qui sont capables de se servir d'un ordinateur pour réaliser une fonction particulière. Par exemple : démarrer un système de bureautique, démarrer un environnement éducatif, démarrer une application de communication, etc.

En français, on peut dire : « médias vifs ». Leur point commun, c'est qu'ils savent démarrer un ordinateur sans utiliser son disque dur, et aussi, même si l'ordinateur ne contient pas de disque dur ou est en panne. Les médias vifs présentés dans **usb.freeduc.org** démarrent en moins d'une minute, et répondent vite. 

	
## Les principale qualités qu'on attend des médias vifs ##

  *  Léger, adaptables, efficaces, robustes ; le démarrage est très rapide.
     *   Le médium vif peut être un CD-ROM, un DVD-ROM, une clé USB, 
	     une carte Flash au format SD, mini-SD, etc. Il est donc léger
		 (quelques grammes)
		 
     *   Les clés USB, les mémoires flash sont robustes. Si 
	     on excepte la sensibilité aux rayures, les CD/DVD le sont aussi ;
		 
     *   Les clés USB et les mémoires flash sont plus
	     flexibles ; on peut les personnaliser et y apporter des mises à jour.
		 
  *  Un usage nomade est possible : le média vif permet d'emporter avec soi
     tout ce qui compte, il s'adapte à tout ordinateur hôte ;
	 
  *  Une grande facilité de diffusion : Freeduc-USB est exclusivement basée
     sur des logiciels libres, la copie est possible sans limite.
	 La duplication est l'affaire de quelques minutes.


# Que faire avec des médias vifs ? #

Les applications possibles sont innombrables, étant donné la très
grande logithèque maintenant disponible dans la distribution Debian
qui sert de base à la production de Freeduc-USB (plus de 60 000
paquets logiciels maintenus en 2019 dans la distribution *stable*).

La base de données de 
<a href="http://www.framasoft.net">Framasoft</a> documente en
français plus de 1 500 logiciels libres destinés à l'utilisateur final.

Voici quelques suggestions :

<dl>
<dt>Des œuvres audio, vidéo avec les logiciels de lecture<dt>

<dd>par exemple :
<ul>
<li>    Un gestionnaire de pistes musicales et une collection fichiers sonores</li>
<li>    Une visionneuse vidéo et quelques films</li>
<li>    Un gestionnaire de collection de photos et vos photos préférées</li>
</ul></dd>

<dt>Une collection de logiciels pour enseigner les sciences</dt>
<dd>S'utilise en classe, et peut être copiée en autant d'exemplaires que d'élèves pour utiliser chez soi, à l'identique</dd>

<dt>Un environnement de création typographique de qualité professionnelle</dt>
<dd>Le système LaTeX, stable depuis maintenant tente ans, est utilisé par
de nombreux professionnels de l'édition de qualité. Ce système s'accommode
très bien des médias vifs.</dd>

<dt>Une borne de consultation interactive (kiosque)</dt>
<dd>Le navigateur Firefox, additionné d'un ou plusieurs greffons (plugins) spécifiques, permet de réaliser :
<ul>
<li>      Un système de visionnage multimédia adapté à des œuvres interactives installées sur le même support : par exemple une borne interactive dans un musée ou une exposition ;</li>
<li>      Un navigateur sous contrôle (parental ou autre), pour accéder à une partie du web ;</li>
<li>      Un système d'examen « scolaire » embarqué, obtenu par association avec un service Wims embarqué sur le même support : utile pour des environnements particuliers, quand Internet n'est pas facilement accessible (pays peu connectés, hôpitaux, prisons, etc.) ...</li>
</ul></dd>
<dt>Un outil très pratique pour des diagnostics et des réparations</dt>
<dd>Comme les médias vifs savent démarrer même quand l'ordinateur est en panne, ils permettent de réparer des disques durs dans des cas extrêmes. Les collections de logiciels libres utilisables dans ce contexte sont très riches, à destination des techniciens.</dd>
</dl>


# Mode d'emploi : j'ai une clé USB vive, et ensuite ... ? #

Le tout premier programme qui démarre dans un ordinateur, avant même
le système d'exploitation (Windows, Mac OSX, GNU/Linux, ou FreeBSD, etc.)
est le programme de démarrage qui est mis en place par le fabricant de
la carte mère, celle-là même qui supporte le processeur. Ces
programmes **BIOS** ou **UEFI** font leur travail
discrètement, ils repèrent les disques disponibles dans l'ordinateur,
et orientent le démarrage vers l'un de ces disques.

## Je choisis la clé USB comme disque de démarrage ##

Au début, la clé USB vive doit être connectée, et l'ordinateur doit être
éteint. Si l'ordinateur est en fonctionnement (par exemple sous Windows),
il suffit de l'éteindre, en laissant la clé USB branchée. Il faut ensuite
s'assurer que c'est bien la clé USB vive qui démarre l'ordinateur.

<dl>
<dt>Si le BIOS permet de choisir la façon de démarrer</dt>
<dd>    Pour de nombreux ordinateurs, Il suffit d'appuyer sur une touche
spéciale au moment du démarrage du BIOS, et il devient alors possible de
choisir l'un des « disques » disponibles. Selon le BIOS du fabricant, cette
touche peut être F8, ou F9 ou encore F12. Après appui sur cette touche
(qui est signalée en bas de l'écran quelques secondes, ou encore dans la
notice de la carte mère de l'ordinateur), un choix apparaît : disque dur,
lecteur de CD-ROM, carte réseau, clé USB. La clé vive se reconnaît
généralement par le mot « USB » et peut-être le nom de son fabricant.</dd>

<dd>    Alors, il suffit de choisir la clé USB à l'aide des touches 
fléchées (haut et bas), puis de valider ! </dd>

<dt>Définir la clé USB vive comme démarrage par défaut</dt>

<dd> On peut aussi faire un pause dans le démarrage, pour changer
les réglages par défaut du BIOS. Là aussi, une touche spéciale est dédiée
à la tâche souvent nommée « BIOS setup ». Selon le BIOS du fabricant, c'est
la touche Suppr, ou Del, ou F2. Après appui sur cette touche (qui est
signalée en bas de l'écran quelques secondes, ou encore dans la notice
de la carte mère de l'ordinateur), un système de menus apparaît. En navigant
parmi ces menus, il faut s'assurer des choses suivantes :
<ul>
<li>        Que le support des périphériques USB est activé durant
le démarrage. Si les périphériques USB sont ignorés, la clé vive ne
sera pas considérée. Cette option peut se trouver dans les réglages
avancés (Advanced settings).</li>
<li>        Il faut examiner les menus relatifs au démarrage (Boot). 
Il est difficile de donner un règle générale qui s'applique à tous les
BIOS existants. Dans certains cas, il suffit d'examiner et de modifier
le menu qui concerne l'ordre des médias pour le démarrage (menu Device
Boot Order), afin que la clé USB devienne prioritaire, et dans
d'autres cas, la clé USB vive a déjà été examinée par le BIOS et elle
est déjà considérée comme un disque dur « ordinaire ». Dans ce cas
elle n'apparaîtra pas dans la liste qui concerne l'ordre des médias,
mais on peut s'intéresser à un liste des disques durs (hard disks) :
il faut alors faire en sorte que le disque « dur » qui porte la nom de la
clé soit plus prioritaire que le vrai disque dur de l'ordinateur.</li>
</ul>
Quand un changement de priorité des disques est fait, il est important de sauvegarderles nouveau paramètres du BIOS avant de redémarrer l'ordinateur.
</dd>
<dt>Le cas des vieux ordinateurs</dt>
<dd> Dans le cas des ordinateurs fabriqués avant 2005, on trouve des
BIOS qui ne savent pas démarrer l'ordinateur à l'aide d'un
périphérique branché sur une prise USB. La parade consiste à démarrer
avec un CD-ROM qui reprend le système de démarrage, la clé USB étant
en place.</dd>
</dl>

## Le démarrage de Freeduc-USB ##
<dl>
<dt>L'écran de choix des options de démarrage</dt>
<dd><img src="{attach}/img/freeduc-boot-menu.png" alt="Écran d'accueil"/></dd>
<dd> Un écran spécial apparaît quand c'est bien la clé vive qui a
pris le contrôle. Si on ne fait rien de spécial, au bout
de quelques secondes c'est la première option qui est prise en compte
pour le démarrage. Avec les flèches du clavier haut et bas, on peut
sélectionner d'autres option.
</dd>
<dt>Les messages émis lors du démarrage</dt>
<dd><img src="{attach}/img/freeduc-boot-startmsg.png" alt="Copie de
l'écran durant la configuration du matériel"/></dd> <dd>Pendant le
démarrage, divers messages s'affichent, qui ponctuent les étapes de la
découverte du matériel de l'ordinateur (Freeduc-USB peut fonctionner
sur de très nombreux types d'ordinateur, et donc la découverte du
matériel présent doit être faite à chaque fois). Au cas où le
démarrage reste bloqué (ça peut arriver), notez bien les derniers
messages qui sont apparus avant le blocage : ils peuvent être utiles
pour résoudre un problème spécifique à l'ordinateur utilisé.</dd>
<dt>Ça y est, Freeduc-USB a démarré</dt>
<dd>Voici un exemple du bureau qui apparaît pendant le fonctionnement
de Freeduc-USB. Dans le cas illustré ci-dessous, c'est un bureau LXDE.

<img src="{attach}/img/freeduc-boot-lxde.png" alt="copie d'écran d'un bureau LXDE"/>

Observez bien le symbole en bas à gauche du bureau 
(<img alt="menu lxde" src="{attach}/img/menu-lxde.png" style="vertical-align: middle;"/>),
c'est en cliquant dessus
qu'on accède aux applications de la clé vive. C'est aussi en cliquant
dessus qu'on peut choisir d'arrêter « proprement » l'ordinateur à la
fin de la session. Il est important d'éteindre l'ordinateur en
utilisant l'application qui gère la bonne cohérence de la clé
USB. </dd>
<dt>Il faut attendre que l'ordinateur soit complètement arrêté avant
de débrancher la clé USB vive.</dt>
<dd>De la même façon qu'il ne vous viendrait pas à l'idée d'ouvrir
votre ordinateur pour en extraire le disque dur pendant le
fonctionnement, ne débranchez en aucun cas la clé USB vive pendant la
session de travail : la cohérence des données est vérifiée au 
prochain démarrage,
mais on pourrait perdre certaines d'entre elles.</dd> 

<dd><img src="{attach}/img/freeduc-boot-endmsg.png" alt="copie de l'écran
d'extinction"/></dd>

<dd>Après avoir demandé l'extinction « propre » de l'ordinateur, ne
retirez la clé USB qu'après avoir lu sur l'écran de fin, les mots
Shutdown complete, ou encore après l'extinction physique de
l'ordinateur, qui survient peu après. </dd> </dl>

# Ce que le projet Freeduc-USB n'est pas : #

  *  Freeduc-USB n'est pas une distribution Linux à installer. Si vous recherchez une distribution GnuLinux de logiciels éducatifs, visitez le projet <a href="http://wiki.debian.org/DebianEdu">Debian-Edu</a>, ou le site web <a href="http://www.framasoft.net/">Framasoft</a>, ...
  *  Freeduc-USB n'est pas une famille de clés USB pour Windows. En fait, Freeduc-USB ne dépend d'aucun logiciel installé localement sur l'ordinateur, on peut démarrer Freeduc-USB sur un ordinateur sans disque. Si vous recherchez des logiciels libres pour Windows, voyez <a href="http://www.framakey.org/">Framakey</a>.
  
# Caractéristiques de Freeduc-USB #

Freeduc-USB est une famille de clés USB vives rapides. Il suffit juste de brancher  une clé Freeduc-USB sur un ordinateur de table ou sur un portable de l'allumer. 

Une demi-minute après, on a unun environnement éducatif, basé sur des logiciels libres. Freeduc-USB permet un usage nomade des applications pédagogiques. Comme c'est basé sur du logiciels libre, on peut le copier sans limite. 

 *   Freeduc-USB est **rapide** : l'ordinateur est prêt à servir plus vite avec Freeduc-USB qu'avec son environnement ordinaire.
 *   Freeduc-USB est **libre** : les licences des applications qu'elle porte autorisent sans problème la duplication.

# Freeduc-USB : jetons un coup d'œil « sous le capot » #
Voici un résumé des sources des logiciels utilisés pour réaliser Freeduc-USB

 *   Le démarrage est pris en charge par <a href="https://fr.wikipedia.org/wiki/Syslinux">SysLinux</a> ;
 *   Le système d'exploitation est <a href="http://www.kernel.org/">GNU-Linux</a> ;
 *   La gestion de l'appliance est basée sur <a href="http://www.knopper.net/knoppix/">KNOPPIX</a> pour les versions anciennes ; Les versions récentes démarrent comme la distribution Debian ordinaire ;
 *   Tous les paquets logiciels sont gérés par <a href="http://fr.wikipedia.org/wiki/Dpkg">dpkg</a>, le système de gestion des paquets Debian ;
 *   L'essentiel des paquets vient des <a href="http://www.debian.org/distrib/packages#search_packages">dépôts officiels Debian</a>.
 
# Structure d'une clé Freeduc-USB #

La structure expliquée ici vaut pour les versions récentes, postérieures à
mars 2019.

Une clé Freeduc USB est structurée en deux partitions, une qui est en
lecture seule, et la seconde qui assure la persistance, et où on peut écrire.
La deuxième partition peut ne pas être vue depuis Windows. Ne l'effacez pas
ou ne la formatez pas sans savoir, même si Windows vous le suggère.

  *  Secteur de boot maître : celui-ci vient du paquet Syslinux, il est réglé
     pour démarrer à partir de la première partition.
  *  Partition 1 : partition primaire ; elle est reconnue comme un DVD-ROM
  *  Partition cachée 2 : nécessaire au démarrage quand l'UEFI est activé
  *  Partition 3 : partition de persistance, avec un système de fichiers `ext4`.
     Cette partition contiendra les personnalisations, l'histoire et les
	 données personnelles de l'utilisateur.

# Versions de Freeduc-USB publiées #

À l'heure actuelle, réaliser une nouvelle version de Freeduc-USB est
une question de quelques heures de travail, grâce au paquet
linux-build.

Voici une liste des versions publiées de Freeduc-USB :

## Freeduc-jbart ##

[Freeduc-jbart](jbart.html) : cette clé est ainsi nommée en hommage au
corsaire Jean Bart, qui au temps de Louis XIV, avait sauvé de la
famine la région de Flandre Littorale en mettant fin à un blocus créé
par la marine anglaise. Cette clé a été développée pour le lycée Jean
Bart de Dunkerque. Elle est utilisée pour l'enseignement de
l'informatique, de la robotique, et des sciences.

## Freeduc-Latix ##
Cette clé en vedette la suite TeXlive. Celle-ci permet de créer vos meilleurs documents, que vous composerez à l'aide du meilleur logiciel de typographie existant, LaTeX. À l'aide de Freeduc-Latix, vous pourrez :

 *   créer des documents courts, des rapports, des articles ;
 *   composer des lettres conformes à la typographie française ;
 *   créer des présentations interactives pour le vidéo-projecteur ;
 *   écrire une thèse, un livre ;
 *   gérer un glossaire, une bibliographie, un index bien structuré ;
 *   ... et bien d'autres choses !
 
## Freeduc-AMC ##
[Freeduc-AMC](amc.html), version 1.0 (août 2010). Met en vedette l'application auto-multiple-choice. Celle-ci permet de créer des exercices de type QCM imprimés sur papier, qu'on peut corriger et noter automatiquement à l'aide d'un scanner. Pour déployer l'appliance, on a besoin d'un ordinateur connecté à une imprimante et un scanner (ou un périphérique combiné qui offre ces deux fonctions). 

## Freeduc-Wims ##
[Freeduc-Wims](wims.html), un service WIMS complet, implanté dans une petite clé USB. Celui-ci permet les activités suivantes :

 *   Wims repose sur un code compact et extrêmement efficace
    Le langage OEF qui sert à développer les exercices est très puissant, et par conséquent la source de chaque exercice est compacte
 *   Comme ça peut se faire, pourquoi ne pas le faire ?
 
### Que faire avec un Wims sur une clé USB ? ###

Quand on a un Wims dans sa poche, prêt à fonctionner sur n'importe quelle machine, voici ce qu'on peut faire :

 *   On peut réaliser des essais, développer un module de façon sécurisée, avant de le publier sur un serveur « en dur ».
 *   On peut réaliser une démonstration à la volée, de façon sûre, à l'aide de n'importe quel ordinateur, qu'il soit ou non connecté à Internet.
 *   On peut reconstruire un service Wims, le temps de quelques heures, dans un réseau local coupé du reste du monde, pour une séance de formation. Détail important : les résultats des évaluations restent conservés dans la clé USB, donc ce mode de fonctionnement est valide y compris pour des conditions d'examen. Voici des exemples de réseaux locaux qui sont ordinairement coupés de l'Internet, ou mal reliés :
    *    Les salles d'ordinateurs dans une classe de campagne, dans un hôpital, dans une prison ;
    *    Les salles d'ordinateurs dans un établissement qui filtre fortement les communications ;
    *    Les salles d'examen.
 *   On peut cloner le service Wims complet, avec toutes ses annexes, en quelques minutes, par une copie de bas niveau de clé à clé.
 *   On peut s'entraîner, et entraîner d'autres personnes, à l'administration d'un service Wims, ce qui serait hors de question, s'agissant d'un service Wims officiel en activité.

## Freeduc-Expo ##

[Freeduc-Expo](expo.html) est un environnement qui permet de visiter une exposition composée de documents visuels et sonores, et éventuellement d'en imprimer certains avec une bonne qualité. À utiliser pour réaliser des bornes interactives de consultation, sans connaissance préalable en informatique.

  *  Première réalisation : une clé vive supportant l'exposition « <a href="http://revolution-francaise.net/2010/04/27/376-exposition-perissent-les-colonies-plutot-un-principe-1">Périssent les colonies plutôt qu'un principe</a> » de Florence Gauthier.

# La première génération des Freeduc-USB vives #

<img src="{attach}/img/cle.jpg" alt="an USB stick"/>

La première clé Freeduc-USB a été marquée au logo d'OFSET, une association
française avec des participants du monde entier, qui avait créé
<a href="https://fr.wikipedia.org/wiki/Freeduc-cd">Freeduc-CD</a>.


# Des affiches de présentation #

<table>
<tr>
  <td text-align="center">
    <a href="{attach}/img/mediavif1.pdf">
      <img src="{attach}/img/mediavif1-thumb.png" alt="affiche1"/></a>
    <br/>Les médias vifs : pourquoi ?</td>
  <td text-align="center">
    <a href="{attach}/img/mediavif3.pdf">
      <img src="{attach}/img/mediavif3-thumb.png" alt="affiche2"/></a>
    <br/>Les médias vifs : comment ?</td>
</tr>
</table>

# Conférences #

  * <a href="http://speeches.ofset.org/georges/2011-patras-DIDAPRO/paper.pdf">Conférence donnée</a> au colloque <a href="http://www.ecedu.upatras.gr/didapro/index.htm">DIDAPRO 4 à Patras en octobre 2011</a>.

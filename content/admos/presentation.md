title:       Une clé vive pour Albert De Mun
subtitle:    version 2021
lang: fr
slug:     admos
author:      Olivier Booklage
date:        2021-05-22
category: Admos

<img src="/img/admos/creteil-logo.png" alt="loga Académie de Créteil"/>
# AdmOS : Un système d'exploitation Linux Debian sur clé USB
#### Pour les Mathématiques, les Sciences Numériques (SNT) et l'Informatique au collège et au lycée (NSI).


**Cette page a été adaptée d'après le [dépôt des sources d'ADMOS](https://salsa.debian.org/obook-guest/admos/) **


|<img src="/img/admos/admos-screencapture.png" alt="Copie d'écran"/>|
|:--:| 
| AdmOS s'installe sur une clé USB ou dans VirtualBox © |

L'image USB vive AdmOS est un projet pour clé USB Live Debian 10 Cinnamon avec persistance d'après le dépôt original de Georges Khaznadar, 
réalisé depuis l'année 2019 à l'aide du paquet **live-build** de Raphaël Hertzog et Daniel Baumann. 
Ce dépôt contient le travail réalisé pour l'enseignement des mathématiques, des Sciences Numériques et Technologie (SNT) et de Numérique et Sciences informatiques (NSI)
à [l'ensemble scolaire Albert de Mun](https://www.albertdemun.fr) à Nogent-sur-Marne.

| <img src="/img/admos/admos-snt01.png" alt="Usage en classe"/> |
|:--:| 
| Chaque élève utilise sa clé personnelle Linux contenant son environnement de travail et ses documents |

Choix de la clé USB
----------------
<img src="/img/admos/usb-stick.png" width="120" alt="une clé USB"/>

Le choix de la clé USB3 est crucial pour un fonctionnement correct d'AdmOS. Pour moins de 10 euros, de bons résultats sans latence ont été obtenus avec le modèle **SanDisk Ultra USB 3.0 Flash Drive 16Go**. Les clés USB bon marché ou optimisées pour le transfert de gros fichiers fonctionneront très lentement, voire pas du tout. Pour une rapidité optimale, préférer l'utilisation d'une carte MicroSD et d'un lecteur de carte microSD vers USB3, par exemple **SanDisk MobileMate**.

| <img src="/img/admos/admos-snt02.png" alt="usage en classe"/> |
|:--:| 
| Il n'y a rien à installer : la clé contient déjà un grand nombre de logiciels comme Python3 et Apache |

Création de la clé USB
-------------------------
1 - Télécharger le fichier admos-cinnamon-amd64.hybrid.iso depuis le site [AdmOS](https://admos.albertdemun.education/). Si vous le pouvez, vérifier son intégrité à l'aide du fichier SHA256SUMS.

2 - "Graver" sur la clé USB le fichier ISO avec un logiciel de création de clé USB bootable comme [Etcher](https://www.balena.io/etcher/), [RosaImageWriter](http://wiki.rosalab.ru/en/index.php/ROSA_ImageWriter), [Rufus](https://rufus.ie/fr_FR.html), etc. Une simple copie du fichier ISO sur la clé ne fonctionnera pas.

Utilisation
-----------
Connecter la clé USB à un ordinateur puis le démarrer. Obtenir l'affichage du menu du périphérique de démarrage en appuyant sur la touche dédiée du clavier. Ce peut-être F12, F11 ou Esc ;  ce processus est différent pour chaque constructeur ou modèle d'ordinateur. Une recherche sur Internet est parfois nécessaire pour connaître cette touche. Dans ce menu de démarrage, sélectionner la clé USB pour lancer le système d'exploitation vif. Si la clé USB n'apparait pas dans le menu, il faut généralement retirer l'option _Secure Boot_ dans la configuration du BIOS, comme dans l'image ci-dessous.

| <img src="/img/admos/admos-bios.png" alt="copie d'écran"/> |
|:--:| 
| Dans le BIOS de l'ordinateur, il faut en général enlever les options _Secure Boot_ et _Fast Boot_ |

Persistance des données
-----------------------
Pour conserver les données sur la clé, il faut procéder selon la notice "Création de la partition de persistance" de la page [Faire une clé vive Freeduc-jbart](/freeduc-usb/freeduc-jbart/index.fr.html) dont voici les étapes.

Une fois la clé avec l'ISO tout juste installée, la retirer et la remettre dans l'ordinateur. Dans une terminal, nous allons l'identifier, créer une 3e partition nommée 'persistence' et y déposer un fichier texte. Soyez prudent sur l'utilisation de fdisk et mkfs car toute erreur peut entraîner l'effacement total de votre système d'exploitation.

Déterminer le chemin de la clé USB. Ce chemin est de la forme /dev/sdX où X désigne une lettre minuscule (a, b, c, etc), à adapter à votre système de fichiers.
```
$ sudo fdisk -l
Disque /dev/sdX : 29,5 GiB, 31625052160 octets, 61767680 secteurs
```
Création d'une 3e partition puis formatage en ext4 avec le nom 'persistence'.
```
$ sudo fdisk /dev/sdX
n
p
3
Entrée+Entrée
w
q
$ sudo mkfs -t ext4 -L persistence /dev/sdX3
```
Les commandes utilisées sont n = new (nouvelle partition), p = primary (partition primaire), 3 = troisième partition, w = write (écrire), q = quit (quitter).

Dans la partition persistence de la clé, créer un fichier texte brut nommé **persistence.conf**, contenant une seule ligne "/ union" (sans les guillemets) MAIS avec un retour à la ligne.

Clonage de la clé avec persistance
----------------------------------
Une fois une clé prête avec sa persistance, elle peut être clonée avec sa persistance (remise à zéro) à l'aide du logiciel 'Freeduc Live Clone' fourni. Cette méthode est pratique pour fabriquer rapidement des clés pour l'ensemble des élèves de la classe.

Utilisation sans clé USB
------------------------
Le système d'exploitation vif AdmOS peut-être utilisé sans clé USB, dans une machine virtuelle comme VirtualBox. 
Il faudra régler l'affichage avec l'accélération 3D. Pour la persistance des données, ajouter le disque SATA fourni dans l'archive [persistence.zip](https://admos.albertdemun.education/persistence.zip)

Principaux logiciels pré-installés
-----------------------

| Nom |   Désignation       |
| ---------- | ---------------------------------------------------------- |
|  apache2 + php + mysql                                |  Serveur web et base de données via php-mysqli |
|  angry ip scanner                                     |  Scanner de réseau |
|  arduino                                              |  Utilitaire graphique de programmation pour Arduino |
|  audacity                                             |  Logiciel d'enregistrement de son numérique et d'édition |
|  avogadro                                             |  Éditeur et un visualiseur avancé de molécules |
|  blender (retiré)                                     |  Logiciel de modélisation, d’animation par ordinateur |
|  cheese                                               |  Capture de photos et de vidéos |
|  chemtool                                             |  Éditeur de molécule 2D |
|  dia                                                  |  Logiciel de création de schéma |
|  eog                                                  |  Visionneuse d'images |
|  emacs25                                              |  Éditeur de texte brut |
|  eyes17                                               |  hardware & software framework for developing science experiments |
|  filius                                               |  Logiciel de simulation de réseaux informatiques |
|  firefox-esr                                          |  Navigateur web |
|  fritzing                                             |  Conception de circuit imprimé |
|  galculator                                           |  Calculatrice |
|  geany                                                |  Éditeur de texte brut et de code source |
|  geogebra                                             |  Logiciel de géométrie dynamique |
|  geophar                                              |  Couteau suisse du prof de maths |
|  ghemical                                             |  Progiciel de chimie computationnelle |
|  gimp                                                 |  Retouche d’image |
|  git                                                  |  Logiciel de versionning pour le développement en équipe |
|  gretl                                                |  Logiciel de statistiques |
|  inkscape                                             |  Logiciel libre de dessin vectoriel |
|  jsmath                                               |  Bibliothèque logicielle en JavaScript pour l'affichage de mathématiques dans un navigateur web |
|  kicad                                                |  Suite logicielle libre de conception pour l'électronique |
|  kino                                                 |  Montage vidéo |
|  krita                                                |  Outil d'édition et de retouche d'image |
|  librecad                                             |  Conception assistée par ordinateur |
|  libreoffice + mythes-fr hunspell-fr hyphen-fr        |  Suite bureautique : éditeur de texte et tableur |
|  logisim                                              |  Simulateur de circuits logiques |
|  mu-editor                                            |  Environnement de développement simplifié pour Micro:Bit |
|  openboard                                            |  Tableau blanc virtuel |
|  optgeo                                               |  Simulation d'optique géométrique à deux dimensions |
|  pyacidobasic                                         |  Logiciel de simulation de dosages chimiques |
|  python3 + spyder + thonny + jupyter-notebook + pyqt5 |  Langage Python3 |
|  python3-mecavideo                                    |  Outil pédagogique d'analyse d'enregistrements vidéo pour la mécanique |
|  qtiplot                                              |  Graphisme scientifique interactif et l'analyse de données |
|  qttools5 + qt5                                       |  Programmation et interfaces, C/C++ |
|  rstudio                                              |  Traitement de données et analyse statistique |
|  ruby                                                 |  Langage open-source dynamique |
|  scribus                                              |  Publication Assistée par Ordinateur |
|  sqlite3 sqlitebrowser                                |  Base de données |
|  stellarium                                           |  Logiciel de planétarium |
|  texmaker                                             |  Éditeur pour LaTeX |
|  thunderbird                                          |  Client courriel |
|  tkgate                                               |  Simulateur de circuit numérique |
|  vlc                                                  |  Lecteur multimedia |
|  wireshark                                            |  Analyseur de paquets réseau |
|  xgnokki                                              |  Connexion téléphone portable |
|  xia                                                  |  Création d’images interactives |
|  zegrapher                                            |  Logiciel de tracé de courbes |

Versions
--------

| Date |   Tag       |    Branche |   Commentaires |
| ----------- | ----------- | ---------- | ---------------------------------------------------------- |
| 2020.01.01 |    v2020.01 |   main | Version utilisable au lycée Albert de Mun à Nogent sur Marne pour l'enseignements des SNT. |
| 2020.01.23 |    v2020.02 |   main | Ajout de Angry IP SCanner et des drivers Wifi realtek (non-free). Bug fixé sur le démarrage en Legacy. |
| 2020.02.13 |    v2020.03 |   main | Ajout d'OpenBoard. |
| 2020.03.02 |    v2020.04 |   main | vlc en français, parcours du réseau dans nemo (gvfs-backends). |
| 2020.03.27 |    v2020.05 |   main | Ajout de apache2 + php + mysql. Dans le dossier personnel, un répertoire spécifique www/ permet de modifier directement les fichiers du serveur web.|
| 2020.04.04 |    v2020.06 |   main | Retrait de Blender pour faire de la place.|
| 2020.04.12 |    v2020.07 |   main | Ajout de logisim, un simulateur de circuits logiques.|
| 2020.06.22 |    v2020.08 |   main | Correction de bug du boot EFI.|
| 2020.12.20 |    v2021.00 |   main | Mise à jour des logiciels |
| 2021.02.15 |    v2021.01 |   main | Création d'un hostname automatique, persistant sur clé persistante |
| 2021.02.18 |    v2021.02 |   main | Ajout de texlive-latex-extra, réglage du hostname, ajout de documentations |
| 2021.XX.XX |    v2021.03 |   main | Prochain version : association *.py avec Spyder3, retrait de blender, ajout de rstudio |


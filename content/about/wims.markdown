title: Freeduc WIMS
lang:en
slug: wims
category: Freeduc-WIMS
date: 2017-12-30
modified: 2024-02-06


#Freeduc-WIMS#

Freeduc-WIMS features the service `Wims`. <img alt="logo Wims" src="{attach}/img/logo-wims.gif"/>

This appliance is a web service, which can be found on many public servers, like <a href="http://www.freeduc.org/fr/wims.auto.u-psud.fr/wims/">Université Paris-Sud</a>, which provides to its users tens of thousands inline exercises, with on-the-fly changed contents, every time one gets an exercise.

##Why a web service from a USB stick?##

Embedding a complex web service on such a fragile component laike a USB stick may look ridiculous, and as a matter of fact it would be a nonsense to run such a service for thounsands of users, when such an unprotected device is in use.

However, installng WIMS on a USB stick is technically possible, for the following reasons:

 *   Wims relies on a compact and very efficient code;
 *   The OEF language which is used to develop exercises is very powerful, so the source of every exercise is compact;
 *   We can do it, so we should. Why not?

##What to do with WIMS on a USB stick?##

When we can have a WIMS service inside our pocket, ready to run on any machine, here is what we can do:

  *  We can make tests and trials, develop a new module on a secure platform, before its publication on a public server;
  *  We can make a demonstration on-the-fly, securely, with whatever computer, be it connected to Internet or not;
  *  We can run a complete WIMS service, during a few hours, in any place, even when no connection to Internet is possible, for example for a training session. Please notice that results of the exercises will be kept on the USB stick, so such a running mode is valid for exams. Here are examples of local area networks which are not connected to Internet, or weakly connected:
     *   Computer rooms in a countryside classroom, in a hospital, in a prison;
     *   Computer rooms in an educational institution which strongly filters the communication;
     *   Exam rooms;
 *   The WIMS service can be clones completely from a USB key to another one in a few minutes (with a low level copy software);
 *   We can train ourselves and other people to administrate a WIMS service, which would be impossible to do with a public active WIMS service.

For other USB sticks with more general profiles, see [Freeduc-USB](category/about).

<img src="{attach}/img/cle.jpg" alt="the stick and its magnetic box" width="100" align="right" style="padding: 20px;"/>
##How to get a Freeduc-WIMS USB stick##

Just go to the [download page](telecharge_utilise.html):
Knowims has been released recently in February 2024 (24.02).

title: Freeduc Expo
lang:en
slug: expo
category: Freeduc-EXPO
date: 2017-12-30
modified: 2017-12-30

An environment which allows one to visit an exposition made of visual and
sound documents, en eventually to print them with a good quality. Can be used to
make interactive kiosks, with no previous computer knowledge.

The first middle scale exposition with the environment Freeduc-Expo was
for the virtual exposition 
"<a href="http://revolution-francaise.net/2010/04/27/376-exposition-perissent-les-colonies-plutot-un-principe-1">Périssent les colonies plutôt qu'un principe</a>",
created by Florence Gauthier.

Here are possible uses: <a href="http://www.freeduc.org/fr/diaporama1/index.html">interactive slideshow</a>, in some previous version.

For other Freeduc-USB with more general profiles, see
[Freeduc-USB](index-en.html).

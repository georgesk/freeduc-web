title: Freeduc Auto-Multiple-Choice
lang:en
slug: amc
category: Freeduc-AMC
date: 2017-12-30
modified: 2017-12-30


#Freeduc-AMC#

Features the package <a href="http://home.gna.org/auto-qcm/auto-multiple-choice/">`auto-multiple-choice`</a>. It allows you to create paper exercises with replies based on multiple-choices, which can be scored automatically with an optical scanner. To deploy the appliance you need locally a computer hooked to a printer and a scanner (or a composite peripheral with both features). See Framasoft's article about auto-multiple-choice: « <a href="http://www.framasoft.net/article4964.html">des vacances pour les enseignants !</a> ».

For other Freeduc-USB sticks and more general features, see [Freeduc-USB](category/about.html).

<img src="{attach}/img/cle.jpg" alt="the stick and its magnetic box" width="100" align="right" style="padding: 20px;"/>
##How to get a Freeduc-AMC stick##
write to the mailing list ofset@ofset.org, with the subject « Freeduc-AMC wanted »
##Snapshots of the appliance##
###The boot menu###
<img src="{attach}/img/snap1.jpg" alt="Extlinux boot"/>

###During the boot stage###
<img src="{attach}/img/snap2.jpg" alt="During the boot stage"/>

###The desktop###
<img src="{attach}/img/snap3.jpg" alt="the desktop"/>

###Auto-multiple-choice with a project loaded###
<img src="{attach}/img/snap4.jpg" alt="auto-multiple-choice with a project loaded"/>

###Verifying the layouts###
<img src="{attach}/img/snap5.jpg" alt="verifying the layouts"/>

###Edition of an exercise sheet###
<img src="{attach}/img/snap6.jpg" alt="edition of an exercise"/>

###Do not forget to shutdown before removing the USB stick###
<img src="{attach}/img/snap7.jpg" alt="Do not forget to shutdown before removing the USB stick"/>

###Snaphost of the shutdown stage###
<img src="{attach}/img/snap8.jpg" alt="snapshot of the shutdown"/>


 *  version 1.0 (August 2010). First release.

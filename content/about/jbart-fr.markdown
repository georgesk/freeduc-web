title: Freeduc Jean Bart
lang:fr
slug: jbart
category: Freeduc-JBART
date: 2019-10-29

#Freeduc-JBART#

Cette clé est ainsi nommée en hommage au corsaire Jean Bart, qui au temps de Louis XIV, avait sauvé de la famine la région de Flandre Littorale en mettant fin à un blocus créé par la marine anglaise. Cette clé a été développée pour 
le <a href="http://www.lyceejeanbart.fr">lycée Jean Bart</a> de Dunkerque. 
Elle est utilisée pour l'enseignement de l'informatique, de la robotique, et des sciences.

#Se procurer cette clé#

Munissez-vous d'une clé USB de 16 giga-octets, puis connectez-vous à 
<a href="ftp://usb.freeduc.org/freeduc-usb/freeduc-jbart/index.fr.html">
ftp://usb.freeduc.org/freeduc-usb/</a>ou
<a href="https://usb.freeduc.org/freeduc-usb/freeduc-jbart/index.fr.html">
https://usb.freeduc.org/freeduc-usb/
</a>
et suivez les instructions.

#Déploiement en nombre#
Si les clés sont gérées par les élèves, chacun la sienne, et pas de problème.
Cependant si c'est l'enseignant qui prête les clés aux élèves, il faut organiser
la distribution est la collecte, le rangement des clés USB. Pour cela, au lycée
Jean Bart, on a créé un 
<a href="coffret-20-freeduc.html">boîtier commode pour vingt clés</a>.

#Quelques copies d'écran#

##Menu de démarrage##

<img src="{attach}/img/lb201903-01.png" alt="copie d'écran"/>

##Pendant le démarrage##

<img src="{attach}/img/lb201903-02.png" alt="copie d'écran"/>

##Le bureau (menu Éducation déplié)##

<img src="{attach}/img/lb201903-03.png" alt="copie d'écran"/>

##N'oubliez pas d'éteindre l'ordinateur proprement avant de retirer la clé USB##

<img src="{attach}/img/lb201903-04.png" alt="copie d'écran"/>

Il est important de fermer proprement une session de travail comportant des fichiers ouverts. En effet, quand il s'agit d'un fonctionnement sur clé USB, le système Linux conserve de nombreuses données en mémoire vive (en cache), et ne les synchronise sur le clé USB que de temps en temps, quand l'occasion se présente, afin d'améliorer la performance générale. Donc si on retire la clé USB à chaud, le risque existe d'y retrouver un système désorganisé... Mais auriez-vous l'idée de retirer à chaud le disque dur de votre ordinateur, en fonctionnement normal ? La clé USB joue ici le rôle du disque dur.

Pour fermer proprement la session, activez le menu en bas à gauche, cliquez sur l'icône d'interrupteur, puis cliquez sur **Shut Down**.

##Copie d'écran durant la phase d'arrêt (20 secondes environ)##

<img src="{attach}/img/lb201903-05.png" alt="copie d'écran"/>

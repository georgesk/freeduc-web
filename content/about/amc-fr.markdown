title: Freeduc Auto-Multiple-Choice
lang:fr
slug: amc
category: Freeduc-AMC
date: 2017-12-30
modified: 2017-12-30


#Freeduc-AMC#

Met en vedette l'application <a href="http://home.gna.org/auto-qcm/auto-multiple-choice/">`auto-multiple-choice`</a>. Celle-ci permet de créer des exercices de type QCM imprimés sur papier, qu'on peut corriger et noter automatiquement à l'aide d'un scanner. Pour déployer l'appliance, on a besoin d'un ordinateur connecté à une imprimante et un scanner (ou un périphérique combiné qui offre ces deux fonctions). Voyez l'article de Framasoft au sujet de auto-multiple-choice : « <a href="http://www.framasoft.net/article4964.html">des vacances pour les enseignants !</a> ».

Pour d'autres clés Freeduc-USB avec des profils plus généralistes, voyez [Freeduc-USB](category/about.html).

<img src="{attach}/img/cle.jpg" alt="the stick and its magnetic box" width="100" align="right" style="padding: 20px;"/>
##Comment se procurer une clé Freeduc-AMC##
Envoyez un courriel sur la liste de diffusion ofset@ofset.org, avec le sujet « wanted: Freeduc-AMC »
##Copies d'écran de l'appliance##
###Le menu de démarrage###
<img src="{attach}/img/snap1.jpg" alt="Extlinux boot"/>

###Pendant le démarrage###
<img src="{attach}/img/snap2.jpg" alt="During the boot stage"/>

###Le bureau (30 secondes après le démarrage)###
<img src="{attach}/img/snap3.jpg" alt="the desktop"/>

###Auto-multiple-choice avec un projet en cours de fonctionnement###
<img src="{attach}/img/snap4.jpg" alt="auto-multiple-choice with a project loaded"/>

###Vérification des mises en page###
<img src="{attach}/img/snap5.jpg" alt="verifying the layouts"/>

###Édition d'une feuille d'exercices###
<img src="{attach}/img/snap6.jpg" alt="edition of an exercise"/>

###N'oubliez pas d'éteindre l'ordinateur proprement avant de retirer la clé USB###
<img src="{attach}/img/snap7.jpg" alt="Do not forget to shutdown before removing the USB stick"/>

###Copie d'écran durant la phase d'arrêt (20 secondes environ)###
<img src="{attach}/img/snap8.jpg" alt="snapshot of the shutdown"/>


 *   version 1.0 (Août 2010). Première version.

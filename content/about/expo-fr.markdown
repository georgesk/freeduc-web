title: Freeduc Expo
lang:fr
slug: expo
category: Freeduc-EXPO
date: 2017-12-30
modified: 2017-12-30

 Un environnement qui permet de visiter une exposition composée de documents visuels et sonores, et éventuellement d'en imprimer certains avec une bonne qualité. À utiliser pour réaliser des bornes interactives de consultation, sans connaissance préalable en informatique.

La première utilisation à moyenne échelle de l'environnement Freeduc-Expo est dédiée à l'exposition virtuelle « <a href="http://revolution-francaise.net/2010/04/27/376-exposition-perissent-les-colonies-plutot-un-principe-1">Périssent les colonies plutôt qu'un principe</a> », montée par l'historienne Florence Gauthier.

Voyez ici une des utilisations possibles : <a href="http://www.freeduc.org/fr/diaporama1/index.html">diaporama interactif</a>, dans une version préliminaire.
Pour d'autres clés Freeduc-USB avec des profils plus généralistes, voyez [Freeduc-USB](index.html).

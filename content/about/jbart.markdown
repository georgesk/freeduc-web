title: Freeduc Jean Bart
lang:en
slug: jbart
category: Freeduc-JBART
date: 2019-05-15
modified: 2019-06-17

#Freeduc-JBART#

The name of this pendrive honors the memory of the corsaire Jean Bart, qui au temps de Louis XIV, who saved from starvation the region of East Fladers by putting an end to a blocus organized by the British Navy. This pendrive is developped for <a href="http://www.lyceejeanbart.fr">lycée Jean Bart</a> of Dunkerque. It is used to teach computer science, robotics, sciences.

#How to get this pendrive#

Take a 16 GB USB stick, browse to
<a href="ftp://usb.freeduc.org/freeduc-usb/freeduc-jbart/index.html">
ftp://usb.freeduc.org/freeduc-usb/</a>or
<a href="https://usb.freeduc.org/freeduc-usb/freeduc-jbart/index.html">
https://usb.freeduc.org/freeduc-usb/
</a>
and follow instructions.

#Screenshots#

##Startup menu##

<img src="{attach}/img/lb201903-01.png" alt="screenshot"/>

##During the boot process##

<img src="{attach}/img/lb201903-02.png" alt="screenshot"/>

##The desktop (Education Menu is unfolded)##

<img src="{attach}/img/lb201903-03.png" alt="screenshot"/>

##Do not forget to shutdown the computer before unpluging the USB stick##

<img src="{attach}/img/lb201903-04.png" alt="screenshot"/>

It is important to close properly a working session with some open files. When it works with a USB drive, Linux caches many data in the RAM, and synchronizes them with the drive eventually, when some opportunity occurs, in order to improve the overall performance. So if one unplugs the "hot" USB drive, chances are that many data will be disordered... However, would you unplug your "hot" hard disk drive, during a normal session? The USB stick plays the role of the hard disk.

To close the session properly, activate the bottom left menu, click on the switch icon, and click later on **Shut Down**.

##Shutdown process (approximative duration 20 seconds)##

<img src="{attach}/img/lb201903-05.png" alt="screenshot"/>

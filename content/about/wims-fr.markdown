title: Freeduc WIMS
lang:fr
slug: wims
category: Freeduc-WIMS
date: 2017-12-30
modified: 2024-02-06


#Freeduc-WIMS#

Freeduc-WIMS met en vedette le service `Wims`. <img alt="logo Wims" src="{attach}/img/logo-wims.gif"/>

Cette application est un service web, que vous pouvez retrouver par exemple à <a href="http://www.freeduc.org/fr/wims.auto.u-psud.fr/wims/">l'Université Paris-Sud</a>, qui offre aux utilisateurs des dizaines de milliers d'exercices en ligne, dont les contenus sont modifiés à chaque consultation.
##Pourquoi un service web sur une clé ?##

Ça peut paraître dérisoire d'embarquer un service web complexe sur un support aussi « fragile » qu'une clé USB, et, en effet, ça n'a pas de sens d'essayer de faire fonctionner un service qui s'adresse à des dizaines de milliers de personnes sur un support aussi peu protégé.

Cependant, installer WIMS sur une clé USB est techniquement possible, pour les raisons suivantes :

 *   Wims repose sur un code compact et extrêment efficace
 *   Le langage OEF qui sert à développer les exercices est très puissant, et par conséquent la source de chaque exercice est compacte
 *   Comme ça peut se faire, pourquoi ne pas le faire ?

##Que faire avec un Wims sur une clé USB ?##

Quand on a un Wims dans sa poche, prêt à fonctionner sur n'importe quelle machine, voici ce qu'on peut faire :

  *  On peut réaliser des essais, développer un module de façon sécurisée, avant de le publier sur un serveur « en dur ».
  *  On peut réaliser une démonstration à la volée, de façon sûre, à l'aide de n'importe quel ordinateur, qu'il soit ou non connecté à Internet.
  *  On peut reconstruire un service Wims, le temps de quelques heures, dans un réseau local coupé du reste du monde, pour une séance de formation. Détail important : les résultats des évalutations restent conservés dans la clé USB, donc ce mode de fonctionnement est valide y compris pour des conditions d'examen. Voici des exemples de réseaux locaux qui sont ordinairement coupés de l'Internet, ou mal reliés :
     *   Les salles d'ordinateurs dans une classe de campagne, dans un hôpital, dans une prison ;
     *   Les salles d'ordinateurs dans un établissement qui filtre fortement les communications ;
     *   Les salles d'examen.
 *   On peut cloner le service Wims complet, avec toutes ses annexes, en quelques minutes, par une copie de bas niveau de clé à clé.
 *   On peut s'entraîner, et entraîner d'autres personnes, à l'administration d'un service Wims, ce qui serait hors de question, s'agissant d'un service wims officiel en activité.

Pour d'autres clés Freeduc-USB avec des profils plus généralistes, voyez [Freeduc-USB](category/about).

<img src="{attach}/img/cle.jpg" alt="the stick and its magnetic box" width="100" align="right" style="padding: 20px;"/>
##Comment se procurer une clé Freeduc-WIMS##

Il suffit d'aller à la [page des téléchargements](telecharge_utilise.html) :
une version de
Knowims a été récemment mise en ligne, en janvier 2024 (choisir l'onglet 01/2024).

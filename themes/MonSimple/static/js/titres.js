/* création d'un index des titres à la volée */
/* -*- coding: utf-8;  -*- */
/*
 * Script table des matières. Ce script est publié sous licence GPL
 * article : http://giminik.developpez.com/articles/javascript-dom/table-des-matieres/
 * date : 2005-09-14
 * http://www.gnu.org/copyleft/gpl.html
 * Vous pouvez le modifier librement et le redistribuer.
 * Merci de m'indiquer tout bug, incompatibilité, amélioration 
 * à giminik   at   redaction-developpez.com
 */

/*************** modifications par Georges Khaznadar <georgesk@ofset.org> ****
 * Changelog:
 *
 * 2010/09/22
 * corrigé un bug dans la fonction inText : dérécursivé la fonction, et
 * ajouté une vérification pour débusquer les valeurs "null".
 *
 * 2010/09/07
 * séparé la fonction makeTitle qui crée le titre.
 * ajout d'un feedback visuel pour dire que le titre est cliquable
 * créé une feuille de style qui permet que le feedback soit visible et dépende de l'état plié/déplié.
 */

/* Cette fonction permet d'afficher/cacher le contenu d'un élément dont on
 * connaît l'identifiant : containerId. Ici, on s'en sert pour cacher la 
 * liste de liens. En même temps, le nom de classe de l'élément titre de
 * la liste est modifié afin de pouvoir lui affecter un style CSS.
 * containerId : l'identifiant de l'élément html à afficher cacher.
 * classOpened : le nom de la classe à donner à l'élément html lorsqu'il est
 *               affiché.
 * classClosed : le nom de la classe à donner à l'élément html lorsqu'il est 
 *               caché.
 */ 

function TCSwap(containerId, classOpened, classClosed) {
    
    var linkList = document.getElementById(containerId).lastChild;
    var listTitle = document.getElementById(containerId);
    if (linkList.style.display != 'none') {
	linkList.style.display = 'none';
	listTitle.className = classClosed;
    }
    else if (linkList.style.display != 'block') {
	linkList.style.display = 'block';
	listTitle.className = classOpened;
    }
}

/* Cette fonction génère la table des matière. Elle construit les éléments
 * html et les insère dans l'arborescence du document.
 *
 * contentId : seuls les titres contenus dans l'élément (et ses sous éléments)
 *             ayant comme id contentId seront utilisés pour la table des matières.
 *             ce doit être un identifiant valide et existant.
 * insertBeforeId : la table des matières sera insérée juste avant l'élément
 *                  portant cet identifiant. ce doit être un identifiant
 *                  valide et existant.
 * containerId : le nom du conteneur sera celui passé en paramètre. cet
 *               identifiant ne doit pas déjà être utilisé dans la page.
 * minHead : par exemple 5 pour titre h5 : les titres hiérarchiquement inférieurs
 *           sont ignorés. doit être compris entre 1 et 6.
 * maxHead : par exemple 2 pour titre h2 : les titres hiérarchiquement supérieurs
 *           sont ignorés. doit être compris entre 1 et 6 et doit être inférieur
 *           à minHead.
 * tableHeadLevel : un titre est inséré pour annoncer la table des matières.
 *                  utilisez 3 pour que le titre de cette table des matières soit
 *                  h3. doit être compris entre 1 et 6.
 * clickable : booléen indique si la table des matières est rétractable sur
 *             l'évènement click. doit prendre comme valeur true ou false.
 *
 * lang : chaîne, langue à utiliser dans l'interface. 'en' par défaut
 */
var unfoldClick = "↧";
var foldClick = "↥";
var contentTableTitle = 'Contents table';

function contentTable(contentId, insertBeforeId, containerId, minHead,
		      maxHead, tableHeadLevel, clickable, lang) {
    if(lang=='fr') {
	contentTableTitle = 'table des matières';
	unfoldClick = "↧";
	foldClick = "↥";
    }
    var anchorName = 'tableDesMatieres';
    var anchorsNumberingBeginning = 0;
    var openedClass = 'ouvert';
    var closedClass = 'ferme';
    
    if (!document.getElementById) return;
    if (!minHead || minHead < 1 || minHead > 6) {
	minHead = 6;
    }
    if (!maxHead || maxHead < 1 || maxHead > minHead) {
	maxHead = 1;
    }
    if (!tableHeadLevel || tableHeadLevel < 1 || tableHeadLevel > 6) {
	tableHeadLevel = 2;
    }
    if (document.getElementById(containerId)) {
	alert(containerId + ' already exists in this page!');
	return;
    }
    else if (!document.getElementById(insertBeforeId)) {
	alert(insertBeforeId + ' is not an existing id!');
	return;
    }
    else if (!document.getElementById(contentId)) {
	alert(contentId + ' is not an existing id!');
	return;
    }
    else {
	var TCContainer = document.createElement('div');
	TCContainer.id = containerId;
	var content = document.getElementById(contentId);
	var chapters = Array();
	headTag(content, chapters);
	if (chapters.length < 2) return;
	var TCTitle = makeTitle(tableHeadLevel,contentTableTitle, clickable);
	TCContainer.appendChild(TCTitle);
	var theList = document.createElement('ul');
	TCContainer.appendChild(theList);
	if (clickable) {
	    TCTitle.onclick = function() { TCSwap(containerId, openedClass, closedClass) };
	    TCContainer.className = openedClass;
	    theList.onclick = function() { TCSwap(containerId, openedClass, closedClass) };
	}
	for (var i = 0; i < chapters.length; i++) {
	    var titleNumber = parseInt(chapters[i].nodeName.charAt(1));
	    if (titleNumber <= minHead && titleNumber >= maxHead) {
		var anItem = document.createElement('li');
		var aLink = document.createElement('a');
		aLink.appendChild(document.createTextNode(inText(chapters[i])));
		anItem.className = chapters[i].nodeName.toLowerCase();
		if (chapters[i].id) {
		    aLink.href = '#' + chapters[i].id;
		} else {
		    do {
			anchorsNumberingBeginning++;
		    } while (document.getElementById(anchorName + anchorsNumberingBeginning))
			chapters[i].id = anchorName + anchorsNumberingBeginning;
		    aLink.href = '#' + chapters[i].id;
		}
		anItem.appendChild(aLink);
		theList.appendChild(anItem);
	    }
	}
	var beforeElement = document.getElementById(insertBeforeId);
	var theParent = beforeElement.parentNode;
	theParent.insertBefore(TCContainer, beforeElement);
    }
}

/* création d'un titre pour la table des matières
 * @param level le niveau (h1, h2 etc.) du titre
 * @param title le texte du titre
 * @param clickable vrai pour que le titre soit cliquable
 * @result un élément DOM contenant le titre
 */
function makeTitle(level,title, clickable){
    var result = document.createElement('h' + level);
    result.appendChild(document.createTextNode(title));
    if (clickable){
	var wrapLink=document.createElement('div');
	wrapLink.className="clickToUnWrap";
	wrapLink.appendChild(document.createTextNode(unfoldClick));
	result.appendChild(wrapLink);
	var wrapLink=document.createElement('div');
	wrapLink.className="clickToWrap";
	wrapLink.appendChild(document.createTextNode(foldClick));
	result.appendChild(wrapLink);
    }
    return result;
}


/* Cette fonction ajoute récursivement la liste des balises d'en-têtes à l'intérieur 
 * d'un noeud dans le tableau passé en paramètres. Afin de conserver l'ordre et de 
 * prendre en compte tous les éléments d'un noeud, cette fonction est récursive.
 * node : Il s'agit du noeud dans lequel on recherche les éléments titre.
 * headArray : Il s'agit du tableau dans lequel on va ajouter les noeuds
 *             des éléments titre Hn.
 */
function headTag(node, headArray) {
    var childrenNumber = node.childNodes.length;
    for (var i = 0; i < childrenNumber; i++) {
	var element = node.childNodes[i];
	var elementName = element.nodeName.toLowerCase();
	if (elementName == 'h1' || elementName == 'h2' || elementName == 'h3' 
	    || elementName == 'h4' || elementName == 'h5' 
	    || elementName == 'h6') {
	    headArray[headArray.length] = element;
	}
	else {
	    headTag(element, headArray);
	}
    }
}

/* Cette fonction retourne le texte contenu dans un noeud, uniquement le texte.
 * Le texte est épuré de toutes les balises intermédiaires.
 * node : le noeud pour lequel on ne souhaite récupérer que la partie textuelle.
 */
function inText(node) {
    var childrenNumber = node.childNodes.length;
    var foundString = "";
    if (childrenNumber == 0) {
	return node.nodeValue;
    }
    else { 
	for (var i = 0; i < childrenNumber; i++) {
	    val= node.childNodes[i].nodeValue;
	    if(val) foundString += val;
	}
	return foundString;
    }
}

/**
 * Shows the contents table, then hides it
 * finds the language by detection of /-xy\\./ in the url
 **/
function dynShow(){
    var lang="fr"; /* default language */
    var url=window.location.href;
    languages=["de","en","es","fr"];
    for (var i=0; i< languages.length; i++){
	var l = languages[i];
	if (url.match(RegExp("-"+l+"\\."))){
	    lang=l;
	}
    }
    contentTable('content', 'tom', 'tdm', 3, 1, 1, true, lang);
    var tdm=$('#tdm');
    window.setTimeout(function(){
	TCSwap('tdm', "ouvert", "ferme");
	tdm.fadeIn(1000);
    }, 1000);
    window.setTimeout(function(){
	tdm.fadeOut(1000);
    }, 0);
}

/**
 * mise à jour des cases à cliquer du tableau
 **/

function tableau_a_jour(){
    $("#images-gz li").each(function(k,v){
	var name = $(v).data("name");
	var dir = "" + $(v).data("dir");
	var dlUrl = "https://usb.freeduc.org/" + dir;
	
	/**
	 * activate a div when it can be a link to a file
	 * @param theclass class of the the DIV containing the anchor
	 * @param text a text to use as the anchor; can be undefined
	 **/
	function activate_if_file(theclass, text){
	    var d = $("#links ." + theclass);
	    var a = d.find("a");
	    var file = $(v).data(theclass);
	    if (file){
		a.attr("href", dlUrl + file);
		a.css("cursor", "pointer");
		d.removeClass("missing");
		d.css("filter","grayscale(0%)");
		d.removeAttr("title");
		if (text) d.find("a span").text(text);
	    } else {
		a.attr("href", "#");
		a.css("cursor", "not-allowed");
		d.addClass("missing");
		d.css("filter","grayscale(100%)");
		d.attr("title", "Unavailable");
		d.find("a span").text("");
	    }
	}

	var regexp = /.*(\d\d)[.-](\d\d).*/;
	var year = dir.replace(regexp,"20$1");
	var month = parseInt(dir.replace(regexp,"$2")) -1;
	var options = {
	    year: 'numeric',
	    month: 'long',
	};
	var date = new Date(year, month).toLocaleDateString('fr-FR', options);
	var size = $(v).data("size");
	var text = "";
	$(v).text(dir.replace(regexp,"$2/20$1"));
	$(v).on("click", function(){
	    $("#images-gz li").removeClass("selected");
	    $(v).addClass("selected");
	    /* lien vers l'image de clé 16 Go */
	    text = "« " + name + " » (" + size + ") " + date;
            activate_if_file("stick", text);
	    /* lien vers l'image iso simple */
	    text = "« " + name + " » (" + size + ") " + date;
            activate_if_file("isoimage", text);
	    /* lien vers les sommes de contrôle */
            activate_if_file("sum");
	    /* lien vers la signature */
            activate_if_file("sign");
	});
    });
    $("#images-gz li").first().trigger("click");
}

$(document).ready(tableau_a_jour);

#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

SITENAME = 'Freeduc'
AUTHOR = 'OFSET'
SITEURL = ''

PATH = 'content'
STATIC_PATHS = ['img']

TIMEZONE = 'Europe/Paris'

DEFAULT_LANG = 'fr'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
LINKS = (('Freeduc-USB', '/usb/'),
         ('Last published thumbdrives', '/last/'),
         ('Why live USB sticks?', '/why/'))

DEFAULT_PAGINATION = 10

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True

## mes modification ##
INDEX_SAVE_AS = 'blog.html'
THEME= "themes/MonSimple"
DISPLAY_CATEGORIES_ON_MENU = True

#! /usr/bin/python3

import urllib.request
from bs4 import BeautifulSoup

import re, os, sys
from datetime import datetime
from jinja2 import Template

class FileInfo:
    """
    Une classe pour récupérer les données de fichiers et de répertoires

    Paramètre du constructeur :
    @param ligne un élément BeautifulSoup de type TR
    """
    
    def __init__(self, ligne):
        self.type = "file"
        data = ligne.find_all("td")
        self.size = data[3].text
        date = data[2].text
        self.date = datetime.strptime(date.strip(), "%Y-%m-%d %H:%M")
        self.name = data[1].text
        return
    
    def __str__(self):
        return "{type:5s}: {perms} {size:13d} {dateformatee:20} {name}".format(
            **self.__dict__, dateformatee=self.date.isoformat())

    @property
    def name_size(self):
        return {"name": self.name, "size": self.size}

class Telechargeable:
    """
    Une classe qui recense les fichiers téléchargeables qui concernent une
    distribution freeduc-usb.

    Paramètres du constructeur:
    @param dir un répertoire qui peut servir de titre
    @param name le nom de la distro
    @param files
    """

    att_re_title = (
        ("isofile",    re.compile(r".*\.iso$"), "ISO"),
        ("hddfile",    re.compile(r".*\.img$"), "HDD"),
        ("stickimage", re.compile(r".*stick.*"), "STICK"),
        ("sourcetar",  re.compile(r"^live-.*\.live\.tar$"), "SOURCE"),
        ("sums",       re.compile(r"^sha512sums.txt$"), "SUMS"),
        ("signature",  re.compile(r"^sha512sums.txt.asc$"), "SIGN"),
    )
    
    def __init__(self, dir, name, fileinfos, basedir=None):
        self.dir = dir
        self.name = name
        self.basedir = basedir or ""
        for att, _, _ in self.att_re_title:
            setattr(self, att, None)
        for ns in [fi.name_size for fi in fileinfos]:
            for att, regex, _ in self.att_re_title:
                if regex.match(ns["name"]):
                    setattr(self, att, ns)
        print(self)
        return

    def __str__(self):
        result = f"=======================| {self.basedir}/{self.dir} |=======================\n"
        for att, _, title in self.att_re_title:
            if getattr(self,att) is not None:
                result += f"{title:6s} : {getattr(self, att)['name']} ({getattr(self, att)['size']})\n"
        return result[:-1]
        
def get_distros(host, basedir):
    """
    Analyse les sous-répertoires de ftp://host/basedir à la recherche
    de fichiers live-* et sha512sums* et renvoie un dictionnaire
    nom du répertoire => structure des fichiers qu'on y trouve
    """
    result = {}
    ### recherche des répertoires
    dirs=[]
    with urllib.request.urlopen(f'https://{host}/{basedir}') as response:
        html = response.read().decode()
        soup = BeautifulSoup(html, 'html.parser')
        dirs = [a.text for a in soup.find_all("a") if "/" in a.text]
    ### recherche des fichiers téléchargeables
    def interessant(element):
        if element:
            return "live-" in element.text or "sha512sum" in element.text
        return False
    
    for d in dirs:
        with urllib.request.urlopen(f'https://{host}/{basedir}/{d}') \
             as response:
            html = response.read().decode()
            soup = BeautifulSoup(html, 'html.parser')
            lignes = [
                tr for tr in soup.find_all("tr") if interessant(tr.find("a"))
            ]
            result[d] = Telechargeable(
                d, "Freeduc", [FileInfo(l) for l in lignes], basedir)
    return result

"""
Là il faudrait ajouter les Knowims
"""

def runtemplates(distros, path = "content/telechargement", write = True):
    """
    Fait fonctionner les modèles JINJA2 pour générer les pages de
    téléchargement
    """
    markdowns = [os.path.join(path,f) for f in os.listdir(path)
                 if re.match(r"^tele.*\.md$",f)]
    result = {}
    for md in markdowns:
        tm = Template(open(md + ".tpl").read())
        new_md = tm.render(
            distros = [distros[k] for k in reversed(sorted(distros.keys()))],
            date = datetime.now().isoformat()
        )
        result[str(md)] = new_md
        if write:
            with open(md, "w") as outfile:
                outfile.write(new_md)
                print("Généré le fichier", md)
    return result

if __name__ == "__main__":
    host = "usb.freeduc.org"
    basedir = "freeduc-usb/freeduc-jbart"
    wimsdir = "freeduc-usb/knowims"
    distros = get_distros(host, basedir)
    runtemplates(distros)
